import 'dart:convert';


class UserGoogleInfo {
  final String email;
  final String googleId;
  final String photoUrl;

  UserGoogleInfo(
    this.email,
    this.googleId,
    this.photoUrl,
  );

  UserGoogleInfo copyWith({
    String email,
    String googleId,
    String photoUrl,
  }) {
    return UserGoogleInfo(
      email ?? this.email,
      googleId ?? this.googleId,
      photoUrl ?? this.photoUrl,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'email': email,
      'googleId': googleId,
      'photoUrl': photoUrl,
    };
  }

  static UserGoogleInfo fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return UserGoogleInfo(
      map['email'],
      map['googleId'],
      map['photoUrl'],
    );
  }

  String toJson() => json.encode(toMap());

  static UserGoogleInfo fromJson(String source) => fromMap(json.decode(source));

  @override
  String toString() => 'UserGoogleInfo(email: $email, googleId: $googleId, photoUrl: $photoUrl)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is UserGoogleInfo &&
      o.email == email &&
      o.googleId == googleId &&
      o.photoUrl == photoUrl;
  }

  @override
  int get hashCode => email.hashCode ^ googleId.hashCode ^ photoUrl.hashCode;
}
