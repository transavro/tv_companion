import 'package:cloudwalker_companion/domain/tv_interaction/models/protos/TvInteraction.pb.dart';
import 'package:cloudwalker_companion/domain/tv_interaction/models/protos/TvInteraction.pbgrpc.dart';
abstract class ITvInteractionFacade{
  Future<bool> keycodeTrigger(KeycodeReq keycodeReq); 
  Future<GetAllAppsResp> getAllTvApps(GetAllAppsReq getAllAppsReq);
  Future<bool> appActionTrigger(AppReq appReq);
  Future<bool> playContentTrigger(PlayReq playReq);
  Future<List<String>> getTvSources();
  Future<bool> switchToTvSource(String sourceName);
  Future<TvDataResp> getTvData(String deviceName);
  Future<TvInteractionServiceClient> connectToTv(String ip, int port);
}