///
//  Generated code. Do not modify.
//  source: TvInteraction.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const App$json = const {
  '1': 'App',
  '2': const [
    const {'1': 'APP_OPEN', '2': 0},
    const {'1': 'APP_UNINSTALL', '2': 1},
  ],
};

const Resp$json = const {
  '1': 'Resp',
};

const Req$json = const {
  '1': 'Req',
};

const KeycodeReq$json = const {
  '1': 'KeycodeReq',
  '2': const [
    const {'1': 'keycode', '3': 1, '4': 1, '5': 9, '10': 'keycode'},
  ],
};

const GetAllAppsReq$json = const {
  '1': 'GetAllAppsReq',
};

const GetAllAppsResp$json = const {
  '1': 'GetAllAppsResp',
  '2': const [
    const {'1': 'app_list', '3': 1, '4': 3, '5': 11, '6': '.main.AppInfo', '10': 'appList'},
  ],
};

const AppInfo$json = const {
  '1': 'AppInfo',
  '2': const [
    const {'1': 'appName', '3': 1, '4': 1, '5': 9, '10': 'appName'},
    const {'1': 'versioName', '3': 2, '4': 1, '5': 9, '10': 'versioName'},
    const {'1': 'packageName', '3': 3, '4': 1, '5': 9, '10': 'packageName'},
  ],
};

const AppReq$json = const {
  '1': 'AppReq',
  '2': const [
    const {'1': 'app', '3': 1, '4': 1, '5': 14, '6': '.main.App', '10': 'app'},
    const {'1': 'package', '3': 2, '4': 1, '5': 9, '10': 'package'},
  ],
};

const PlayReq$json = const {
  '1': 'PlayReq',
  '2': const [
    const {'1': 'package', '3': 1, '4': 1, '5': 9, '10': 'package'},
    const {'1': 'deepLink', '3': 2, '4': 1, '5': 9, '10': 'deepLink'},
  ],
};

const TvSources$json = const {
  '1': 'TvSources',
  '2': const [
    const {'1': 'tvsources', '3': 1, '4': 3, '5': 9, '10': 'tvsources'},
  ],
};

const TvActionReq$json = const {
  '1': 'TvActionReq',
  '2': const [
    const {'1': 'source', '3': 1, '4': 1, '5': 9, '10': 'source'},
  ],
};

const TvDataReq$json = const {
  '1': 'TvDataReq',
  '2': const [
    const {'1': 'targetModel', '3': 1, '4': 1, '5': 9, '10': 'targetModel'},
  ],
};

const TvDataResp$json = const {
  '1': 'TvDataResp',
  '2': const [
    const {'1': 'data', '3': 1, '4': 3, '5': 11, '6': '.main.TvDataResp.DataEntry', '10': 'data'},
  ],
  '3': const [TvDataResp_DataEntry$json],
};

const TvDataResp_DataEntry$json = const {
  '1': 'DataEntry',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
  '7': const {'7': true},
};

