///
//  Generated code. Do not modify.
//  source: TvInteraction.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'TvInteraction.pb.dart' as $0;
export 'TvInteraction.pb.dart';

class TvInteractionServiceClient extends $grpc.Client {
  static final _$keycodeAction = $grpc.ClientMethod<$0.KeycodeReq, $0.Resp>(
      '/main.TvInteractionService/KeycodeAction',
      ($0.KeycodeReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Resp.fromBuffer(value));
  static final _$getAllApps =
      $grpc.ClientMethod<$0.GetAllAppsReq, $0.GetAllAppsResp>(
          '/main.TvInteractionService/GetAllApps',
          ($0.GetAllAppsReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.GetAllAppsResp.fromBuffer(value));
  static final _$appAction = $grpc.ClientMethod<$0.AppReq, $0.Resp>(
      '/main.TvInteractionService/AppAction',
      ($0.AppReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Resp.fromBuffer(value));
  static final _$playContent = $grpc.ClientMethod<$0.PlayReq, $0.Resp>(
      '/main.TvInteractionService/PlayContent',
      ($0.PlayReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Resp.fromBuffer(value));
  static final _$getListOfTvSources = $grpc.ClientMethod<$0.Req, $0.TvSources>(
      '/main.TvInteractionService/GetListOfTvSources',
      ($0.Req value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.TvSources.fromBuffer(value));
  static final _$tvAction = $grpc.ClientMethod<$0.TvActionReq, $0.Resp>(
      '/main.TvInteractionService/TvAction',
      ($0.TvActionReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Resp.fromBuffer(value));
  static final _$getTvData = $grpc.ClientMethod<$0.TvDataReq, $0.TvDataResp>(
      '/main.TvInteractionService/GetTvData',
      ($0.TvDataReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.TvDataResp.fromBuffer(value));

  TvInteractionServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$0.Resp> keycodeAction($0.KeycodeReq request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$keycodeAction, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.GetAllAppsResp> getAllApps($0.GetAllAppsReq request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$getAllApps, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Resp> appAction($0.AppReq request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$appAction, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Resp> playContent($0.PlayReq request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$playContent, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.TvSources> getListOfTvSources($0.Req request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$getListOfTvSources, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Resp> tvAction($0.TvActionReq request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$tvAction, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.TvDataResp> getTvData($0.TvDataReq request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$getTvData, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class TvInteractionServiceBase extends $grpc.Service {
  $core.String get $name => 'main.TvInteractionService';

  TvInteractionServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.KeycodeReq, $0.Resp>(
        'KeycodeAction',
        keycodeAction_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.KeycodeReq.fromBuffer(value),
        ($0.Resp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetAllAppsReq, $0.GetAllAppsResp>(
        'GetAllApps',
        getAllApps_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.GetAllAppsReq.fromBuffer(value),
        ($0.GetAllAppsResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.AppReq, $0.Resp>(
        'AppAction',
        appAction_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.AppReq.fromBuffer(value),
        ($0.Resp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PlayReq, $0.Resp>(
        'PlayContent',
        playContent_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PlayReq.fromBuffer(value),
        ($0.Resp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Req, $0.TvSources>(
        'GetListOfTvSources',
        getListOfTvSources_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Req.fromBuffer(value),
        ($0.TvSources value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TvActionReq, $0.Resp>(
        'TvAction',
        tvAction_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TvActionReq.fromBuffer(value),
        ($0.Resp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TvDataReq, $0.TvDataResp>(
        'GetTvData',
        getTvData_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TvDataReq.fromBuffer(value),
        ($0.TvDataResp value) => value.writeToBuffer()));
  }

  $async.Future<$0.Resp> keycodeAction_Pre(
      $grpc.ServiceCall call, $async.Future<$0.KeycodeReq> request) async {
    return keycodeAction(call, await request);
  }

  $async.Future<$0.GetAllAppsResp> getAllApps_Pre(
      $grpc.ServiceCall call, $async.Future<$0.GetAllAppsReq> request) async {
    return getAllApps(call, await request);
  }

  $async.Future<$0.Resp> appAction_Pre(
      $grpc.ServiceCall call, $async.Future<$0.AppReq> request) async {
    return appAction(call, await request);
  }

  $async.Future<$0.Resp> playContent_Pre(
      $grpc.ServiceCall call, $async.Future<$0.PlayReq> request) async {
    return playContent(call, await request);
  }

  $async.Future<$0.TvSources> getListOfTvSources_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Req> request) async {
    return getListOfTvSources(call, await request);
  }

  $async.Future<$0.Resp> tvAction_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TvActionReq> request) async {
    return tvAction(call, await request);
  }

  $async.Future<$0.TvDataResp> getTvData_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TvDataReq> request) async {
    return getTvData(call, await request);
  }

  $async.Future<$0.Resp> keycodeAction(
      $grpc.ServiceCall call, $0.KeycodeReq request);
  $async.Future<$0.GetAllAppsResp> getAllApps(
      $grpc.ServiceCall call, $0.GetAllAppsReq request);
  $async.Future<$0.Resp> appAction($grpc.ServiceCall call, $0.AppReq request);
  $async.Future<$0.Resp> playContent(
      $grpc.ServiceCall call, $0.PlayReq request);
  $async.Future<$0.TvSources> getListOfTvSources(
      $grpc.ServiceCall call, $0.Req request);
  $async.Future<$0.Resp> tvAction(
      $grpc.ServiceCall call, $0.TvActionReq request);
  $async.Future<$0.TvDataResp> getTvData(
      $grpc.ServiceCall call, $0.TvDataReq request);
}
