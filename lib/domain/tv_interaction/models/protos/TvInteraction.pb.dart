///
//  Generated code. Do not modify.
//  source: TvInteraction.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'TvInteraction.pbenum.dart';

export 'TvInteraction.pbenum.dart';

class Resp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Resp', package: const $pb.PackageName('main'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Resp._() : super();
  factory Resp() => create();
  factory Resp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Resp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Resp clone() => Resp()..mergeFromMessage(this);
  Resp copyWith(void Function(Resp) updates) => super.copyWith((message) => updates(message as Resp));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Resp create() => Resp._();
  Resp createEmptyInstance() => create();
  static $pb.PbList<Resp> createRepeated() => $pb.PbList<Resp>();
  @$core.pragma('dart2js:noInline')
  static Resp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Resp>(create);
  static Resp _defaultInstance;
}

class Req extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Req', package: const $pb.PackageName('main'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Req._() : super();
  factory Req() => create();
  factory Req.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Req.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Req clone() => Req()..mergeFromMessage(this);
  Req copyWith(void Function(Req) updates) => super.copyWith((message) => updates(message as Req));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Req create() => Req._();
  Req createEmptyInstance() => create();
  static $pb.PbList<Req> createRepeated() => $pb.PbList<Req>();
  @$core.pragma('dart2js:noInline')
  static Req getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Req>(create);
  static Req _defaultInstance;
}

class KeycodeReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('KeycodeReq', package: const $pb.PackageName('main'), createEmptyInstance: create)
    ..aOS(1, 'keycode')
    ..hasRequiredFields = false
  ;

  KeycodeReq._() : super();
  factory KeycodeReq() => create();
  factory KeycodeReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory KeycodeReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  KeycodeReq clone() => KeycodeReq()..mergeFromMessage(this);
  KeycodeReq copyWith(void Function(KeycodeReq) updates) => super.copyWith((message) => updates(message as KeycodeReq));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static KeycodeReq create() => KeycodeReq._();
  KeycodeReq createEmptyInstance() => create();
  static $pb.PbList<KeycodeReq> createRepeated() => $pb.PbList<KeycodeReq>();
  @$core.pragma('dart2js:noInline')
  static KeycodeReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<KeycodeReq>(create);
  static KeycodeReq _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get keycode => $_getSZ(0);
  @$pb.TagNumber(1)
  set keycode($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasKeycode() => $_has(0);
  @$pb.TagNumber(1)
  void clearKeycode() => clearField(1);
}

class GetAllAppsReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('GetAllAppsReq', package: const $pb.PackageName('main'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  GetAllAppsReq._() : super();
  factory GetAllAppsReq() => create();
  factory GetAllAppsReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAllAppsReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  GetAllAppsReq clone() => GetAllAppsReq()..mergeFromMessage(this);
  GetAllAppsReq copyWith(void Function(GetAllAppsReq) updates) => super.copyWith((message) => updates(message as GetAllAppsReq));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAllAppsReq create() => GetAllAppsReq._();
  GetAllAppsReq createEmptyInstance() => create();
  static $pb.PbList<GetAllAppsReq> createRepeated() => $pb.PbList<GetAllAppsReq>();
  @$core.pragma('dart2js:noInline')
  static GetAllAppsReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAllAppsReq>(create);
  static GetAllAppsReq _defaultInstance;
}

class GetAllAppsResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('GetAllAppsResp', package: const $pb.PackageName('main'), createEmptyInstance: create)
    ..pc<AppInfo>(1, 'appList', $pb.PbFieldType.PM, subBuilder: AppInfo.create)
    ..hasRequiredFields = false
  ;

  GetAllAppsResp._() : super();
  factory GetAllAppsResp() => create();
  factory GetAllAppsResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetAllAppsResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  GetAllAppsResp clone() => GetAllAppsResp()..mergeFromMessage(this);
  GetAllAppsResp copyWith(void Function(GetAllAppsResp) updates) => super.copyWith((message) => updates(message as GetAllAppsResp));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetAllAppsResp create() => GetAllAppsResp._();
  GetAllAppsResp createEmptyInstance() => create();
  static $pb.PbList<GetAllAppsResp> createRepeated() => $pb.PbList<GetAllAppsResp>();
  @$core.pragma('dart2js:noInline')
  static GetAllAppsResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetAllAppsResp>(create);
  static GetAllAppsResp _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<AppInfo> get appList => $_getList(0);
}

class AppInfo extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AppInfo', package: const $pb.PackageName('main'), createEmptyInstance: create)
    ..aOS(1, 'appName', protoName: 'appName')
    ..aOS(2, 'versioName', protoName: 'versioName')
    ..aOS(3, 'packageName', protoName: 'packageName')
    ..hasRequiredFields = false
  ;

  AppInfo._() : super();
  factory AppInfo() => create();
  factory AppInfo.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AppInfo.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  AppInfo clone() => AppInfo()..mergeFromMessage(this);
  AppInfo copyWith(void Function(AppInfo) updates) => super.copyWith((message) => updates(message as AppInfo));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AppInfo create() => AppInfo._();
  AppInfo createEmptyInstance() => create();
  static $pb.PbList<AppInfo> createRepeated() => $pb.PbList<AppInfo>();
  @$core.pragma('dart2js:noInline')
  static AppInfo getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AppInfo>(create);
  static AppInfo _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get appName => $_getSZ(0);
  @$pb.TagNumber(1)
  set appName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAppName() => $_has(0);
  @$pb.TagNumber(1)
  void clearAppName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get versioName => $_getSZ(1);
  @$pb.TagNumber(2)
  set versioName($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasVersioName() => $_has(1);
  @$pb.TagNumber(2)
  void clearVersioName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get packageName => $_getSZ(2);
  @$pb.TagNumber(3)
  set packageName($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPackageName() => $_has(2);
  @$pb.TagNumber(3)
  void clearPackageName() => clearField(3);
}

class AppReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AppReq', package: const $pb.PackageName('main'), createEmptyInstance: create)
    ..e<App>(1, 'app', $pb.PbFieldType.OE, defaultOrMaker: App.APP_OPEN, valueOf: App.valueOf, enumValues: App.values)
    ..aOS(2, 'package')
    ..hasRequiredFields = false
  ;

  AppReq._() : super();
  factory AppReq() => create();
  factory AppReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AppReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  AppReq clone() => AppReq()..mergeFromMessage(this);
  AppReq copyWith(void Function(AppReq) updates) => super.copyWith((message) => updates(message as AppReq));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AppReq create() => AppReq._();
  AppReq createEmptyInstance() => create();
  static $pb.PbList<AppReq> createRepeated() => $pb.PbList<AppReq>();
  @$core.pragma('dart2js:noInline')
  static AppReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AppReq>(create);
  static AppReq _defaultInstance;

  @$pb.TagNumber(1)
  App get app => $_getN(0);
  @$pb.TagNumber(1)
  set app(App v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasApp() => $_has(0);
  @$pb.TagNumber(1)
  void clearApp() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get package => $_getSZ(1);
  @$pb.TagNumber(2)
  set package($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPackage() => $_has(1);
  @$pb.TagNumber(2)
  void clearPackage() => clearField(2);
}

class PlayReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('PlayReq', package: const $pb.PackageName('main'), createEmptyInstance: create)
    ..aOS(1, 'package')
    ..aOS(2, 'deepLink', protoName: 'deepLink')
    ..hasRequiredFields = false
  ;

  PlayReq._() : super();
  factory PlayReq() => create();
  factory PlayReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PlayReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  PlayReq clone() => PlayReq()..mergeFromMessage(this);
  PlayReq copyWith(void Function(PlayReq) updates) => super.copyWith((message) => updates(message as PlayReq));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PlayReq create() => PlayReq._();
  PlayReq createEmptyInstance() => create();
  static $pb.PbList<PlayReq> createRepeated() => $pb.PbList<PlayReq>();
  @$core.pragma('dart2js:noInline')
  static PlayReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PlayReq>(create);
  static PlayReq _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get package => $_getSZ(0);
  @$pb.TagNumber(1)
  set package($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPackage() => $_has(0);
  @$pb.TagNumber(1)
  void clearPackage() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get deepLink => $_getSZ(1);
  @$pb.TagNumber(2)
  set deepLink($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDeepLink() => $_has(1);
  @$pb.TagNumber(2)
  void clearDeepLink() => clearField(2);
}

class TvSources extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('TvSources', package: const $pb.PackageName('main'), createEmptyInstance: create)
    ..pPS(1, 'tvsources')
    ..hasRequiredFields = false
  ;

  TvSources._() : super();
  factory TvSources() => create();
  factory TvSources.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TvSources.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  TvSources clone() => TvSources()..mergeFromMessage(this);
  TvSources copyWith(void Function(TvSources) updates) => super.copyWith((message) => updates(message as TvSources));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TvSources create() => TvSources._();
  TvSources createEmptyInstance() => create();
  static $pb.PbList<TvSources> createRepeated() => $pb.PbList<TvSources>();
  @$core.pragma('dart2js:noInline')
  static TvSources getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TvSources>(create);
  static TvSources _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.String> get tvsources => $_getList(0);
}

class TvActionReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('TvActionReq', package: const $pb.PackageName('main'), createEmptyInstance: create)
    ..aOS(1, 'source')
    ..hasRequiredFields = false
  ;

  TvActionReq._() : super();
  factory TvActionReq() => create();
  factory TvActionReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TvActionReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  TvActionReq clone() => TvActionReq()..mergeFromMessage(this);
  TvActionReq copyWith(void Function(TvActionReq) updates) => super.copyWith((message) => updates(message as TvActionReq));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TvActionReq create() => TvActionReq._();
  TvActionReq createEmptyInstance() => create();
  static $pb.PbList<TvActionReq> createRepeated() => $pb.PbList<TvActionReq>();
  @$core.pragma('dart2js:noInline')
  static TvActionReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TvActionReq>(create);
  static TvActionReq _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get source => $_getSZ(0);
  @$pb.TagNumber(1)
  set source($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSource() => $_has(0);
  @$pb.TagNumber(1)
  void clearSource() => clearField(1);
}

class TvDataReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('TvDataReq', package: const $pb.PackageName('main'), createEmptyInstance: create)
    ..aOS(1, 'targetModel', protoName: 'targetModel')
    ..hasRequiredFields = false
  ;

  TvDataReq._() : super();
  factory TvDataReq() => create();
  factory TvDataReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TvDataReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  TvDataReq clone() => TvDataReq()..mergeFromMessage(this);
  TvDataReq copyWith(void Function(TvDataReq) updates) => super.copyWith((message) => updates(message as TvDataReq));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TvDataReq create() => TvDataReq._();
  TvDataReq createEmptyInstance() => create();
  static $pb.PbList<TvDataReq> createRepeated() => $pb.PbList<TvDataReq>();
  @$core.pragma('dart2js:noInline')
  static TvDataReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TvDataReq>(create);
  static TvDataReq _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get targetModel => $_getSZ(0);
  @$pb.TagNumber(1)
  set targetModel($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTargetModel() => $_has(0);
  @$pb.TagNumber(1)
  void clearTargetModel() => clearField(1);
}

class TvDataResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('TvDataResp', package: const $pb.PackageName('main'), createEmptyInstance: create)
    ..m<$core.String, $core.String>(1, 'data', entryClassName: 'TvDataResp.DataEntry', keyFieldType: $pb.PbFieldType.OS, valueFieldType: $pb.PbFieldType.OS, packageName: const $pb.PackageName('main'))
    ..hasRequiredFields = false
  ;

  TvDataResp._() : super();
  factory TvDataResp() => create();
  factory TvDataResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TvDataResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  TvDataResp clone() => TvDataResp()..mergeFromMessage(this);
  TvDataResp copyWith(void Function(TvDataResp) updates) => super.copyWith((message) => updates(message as TvDataResp));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TvDataResp create() => TvDataResp._();
  TvDataResp createEmptyInstance() => create();
  static $pb.PbList<TvDataResp> createRepeated() => $pb.PbList<TvDataResp>();
  @$core.pragma('dart2js:noInline')
  static TvDataResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TvDataResp>(create);
  static TvDataResp _defaultInstance;

  @$pb.TagNumber(1)
  $core.Map<$core.String, $core.String> get data => $_getMap(0);
}

