import 'package:cloudwalker_companion/domain/cw_registration/models/protos/User.pb.dart';
import 'package:cloudwalker_companion/domain/google_sign_in/models/user_google_info.dart';

abstract class IAuthRegistrationFacade{

  Future<User> getLogedInUser();
  Future<UserGoogleInfo> signInWithGoogle();

  Future<User> createUser(User user);
  Future<User> getUser(String googleId);
  Future<User> updateUser(User user);
  Future<bool> deleteUser(String googleId);
  
  Future<List<LinkedDevice>> getLinkedDevices(String googleId);
  Future<LinkedDevice> addLinkedDevice(LinkedDevice linkedDevice);
  Future<bool> removeLinkedDevice(String tvEmac);

}