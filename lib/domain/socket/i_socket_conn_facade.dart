abstract class ISocketConnFacade{
  void triggerKeycode(int keycode);
  void triggerChar(int position, String char);
}