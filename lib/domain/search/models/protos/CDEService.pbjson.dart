///
//  Generated code. Do not modify.
//  source: CDEService.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const TileType$json = const {
  '1': 'TileType',
  '2': const [
    const {'1': 'ImageTile', '2': 0},
    const {'1': 'VideoTile', '2': 1},
    const {'1': 'FeatureTile', '2': 2},
    const {'1': 'AdvertiseTile', '2': 3},
    const {'1': 'CarouselTile', '2': 4},
  ],
};

const Monetize$json = const {
  '1': 'Monetize',
  '2': const [
    const {'1': 'Free', '2': 0},
    const {'1': 'Paid', '2': 1},
    const {'1': 'Subscription', '2': 2},
    const {'1': 'Rent', '2': 3},
  ],
};

const SearchQuery$json = const {
  '1': 'SearchQuery',
  '2': const [
    const {'1': 'query', '3': 1, '4': 1, '5': 9, '10': 'query'},
  ],
};

const SearchResponse$json = const {
  '1': 'SearchResponse',
  '2': const [
    const {'1': 'contentTile', '3': 1, '4': 3, '5': 11, '6': '.cloudwalker.Content', '10': 'contentTile'},
  ],
};

const SearchTextResponse$json = const {
  '1': 'SearchTextResponse',
  '2': const [
    const {'1': 'result', '3': 1, '4': 3, '5': 9, '10': 'result'},
  ],
};

const Content$json = const {
  '1': 'Content',
  '2': const [
    const {'1': 'title', '3': 8, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'poster', '3': 1, '4': 3, '5': 9, '10': 'poster'},
    const {'1': 'portriat', '3': 2, '4': 3, '5': 9, '10': 'portriat'},
    const {'1': 'type', '3': 3, '4': 1, '5': 14, '6': '.cloudwalker.TileType', '10': 'type'},
    const {'1': 'isDetailPage', '3': 4, '4': 1, '5': 8, '10': 'isDetailPage'},
    const {'1': 'contentId', '3': 5, '4': 1, '5': 9, '10': 'contentId'},
    const {'1': 'play', '3': 6, '4': 3, '5': 11, '6': '.cloudwalker.Play', '10': 'play'},
    const {'1': 'video', '3': 7, '4': 3, '5': 9, '10': 'video'},
  ],
};

const Play$json = const {
  '1': 'Play',
  '2': const [
    const {'1': 'monetize', '3': 5, '4': 1, '5': 14, '6': '.cloudwalker.Monetize', '10': 'monetize'},
    const {'1': 'target', '3': 1, '4': 1, '5': 9, '10': 'target'},
    const {'1': 'source', '3': 2, '4': 1, '5': 9, '10': 'source'},
    const {'1': 'package', '3': 3, '4': 1, '5': 9, '10': 'package'},
    const {'1': 'type', '3': 4, '4': 1, '5': 9, '10': 'type'},
  ],
};

