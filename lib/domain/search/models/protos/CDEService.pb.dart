///
//  Generated code. Do not modify.
//  source: CDEService.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'CDEService.pbenum.dart';

export 'CDEService.pbenum.dart';

class SearchQuery extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SearchQuery', package: const $pb.PackageName('cloudwalker'), createEmptyInstance: create)
    ..aOS(1, 'query')
    ..hasRequiredFields = false
  ;

  SearchQuery._() : super();
  factory SearchQuery() => create();
  factory SearchQuery.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SearchQuery.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SearchQuery clone() => SearchQuery()..mergeFromMessage(this);
  SearchQuery copyWith(void Function(SearchQuery) updates) => super.copyWith((message) => updates(message as SearchQuery));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SearchQuery create() => SearchQuery._();
  SearchQuery createEmptyInstance() => create();
  static $pb.PbList<SearchQuery> createRepeated() => $pb.PbList<SearchQuery>();
  @$core.pragma('dart2js:noInline')
  static SearchQuery getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SearchQuery>(create);
  static SearchQuery _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get query => $_getSZ(0);
  @$pb.TagNumber(1)
  set query($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuery() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuery() => clearField(1);
}

class SearchResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SearchResponse', package: const $pb.PackageName('cloudwalker'), createEmptyInstance: create)
    ..pc<Content>(1, 'contentTile', $pb.PbFieldType.PM, protoName: 'contentTile', subBuilder: Content.create)
    ..hasRequiredFields = false
  ;

  SearchResponse._() : super();
  factory SearchResponse() => create();
  factory SearchResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SearchResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SearchResponse clone() => SearchResponse()..mergeFromMessage(this);
  SearchResponse copyWith(void Function(SearchResponse) updates) => super.copyWith((message) => updates(message as SearchResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SearchResponse create() => SearchResponse._();
  SearchResponse createEmptyInstance() => create();
  static $pb.PbList<SearchResponse> createRepeated() => $pb.PbList<SearchResponse>();
  @$core.pragma('dart2js:noInline')
  static SearchResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SearchResponse>(create);
  static SearchResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Content> get contentTile => $_getList(0);
}

class SearchTextResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SearchTextResponse', package: const $pb.PackageName('cloudwalker'), createEmptyInstance: create)
    ..pPS(1, 'result')
    ..hasRequiredFields = false
  ;

  SearchTextResponse._() : super();
  factory SearchTextResponse() => create();
  factory SearchTextResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SearchTextResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SearchTextResponse clone() => SearchTextResponse()..mergeFromMessage(this);
  SearchTextResponse copyWith(void Function(SearchTextResponse) updates) => super.copyWith((message) => updates(message as SearchTextResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SearchTextResponse create() => SearchTextResponse._();
  SearchTextResponse createEmptyInstance() => create();
  static $pb.PbList<SearchTextResponse> createRepeated() => $pb.PbList<SearchTextResponse>();
  @$core.pragma('dart2js:noInline')
  static SearchTextResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SearchTextResponse>(create);
  static SearchTextResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.String> get result => $_getList(0);
}

class Content extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Content', package: const $pb.PackageName('cloudwalker'), createEmptyInstance: create)
    ..pPS(1, 'poster')
    ..pPS(2, 'portriat')
    ..e<TileType>(3, 'type', $pb.PbFieldType.OE, defaultOrMaker: TileType.ImageTile, valueOf: TileType.valueOf, enumValues: TileType.values)
    ..aOB(4, 'isDetailPage', protoName: 'isDetailPage')
    ..aOS(5, 'contentId', protoName: 'contentId')
    ..pc<Play>(6, 'play', $pb.PbFieldType.PM, subBuilder: Play.create)
    ..pPS(7, 'video')
    ..aOS(8, 'title')
    ..hasRequiredFields = false
  ;

  Content._() : super();
  factory Content() => create();
  factory Content.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Content.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Content clone() => Content()..mergeFromMessage(this);
  Content copyWith(void Function(Content) updates) => super.copyWith((message) => updates(message as Content));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Content create() => Content._();
  Content createEmptyInstance() => create();
  static $pb.PbList<Content> createRepeated() => $pb.PbList<Content>();
  @$core.pragma('dart2js:noInline')
  static Content getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Content>(create);
  static Content _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.String> get poster => $_getList(0);

  @$pb.TagNumber(2)
  $core.List<$core.String> get portriat => $_getList(1);

  @$pb.TagNumber(3)
  TileType get type => $_getN(2);
  @$pb.TagNumber(3)
  set type(TileType v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasType() => $_has(2);
  @$pb.TagNumber(3)
  void clearType() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get isDetailPage => $_getBF(3);
  @$pb.TagNumber(4)
  set isDetailPage($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasIsDetailPage() => $_has(3);
  @$pb.TagNumber(4)
  void clearIsDetailPage() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get contentId => $_getSZ(4);
  @$pb.TagNumber(5)
  set contentId($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasContentId() => $_has(4);
  @$pb.TagNumber(5)
  void clearContentId() => clearField(5);

  @$pb.TagNumber(6)
  $core.List<Play> get play => $_getList(5);

  @$pb.TagNumber(7)
  $core.List<$core.String> get video => $_getList(6);

  @$pb.TagNumber(8)
  $core.String get title => $_getSZ(7);
  @$pb.TagNumber(8)
  set title($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasTitle() => $_has(7);
  @$pb.TagNumber(8)
  void clearTitle() => clearField(8);
}

class Play extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Play', package: const $pb.PackageName('cloudwalker'), createEmptyInstance: create)
    ..aOS(1, 'target')
    ..aOS(2, 'source')
    ..aOS(3, 'package')
    ..aOS(4, 'type')
    ..e<Monetize>(5, 'monetize', $pb.PbFieldType.OE, defaultOrMaker: Monetize.Free, valueOf: Monetize.valueOf, enumValues: Monetize.values)
    ..hasRequiredFields = false
  ;

  Play._() : super();
  factory Play() => create();
  factory Play.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Play.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Play clone() => Play()..mergeFromMessage(this);
  Play copyWith(void Function(Play) updates) => super.copyWith((message) => updates(message as Play));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Play create() => Play._();
  Play createEmptyInstance() => create();
  static $pb.PbList<Play> createRepeated() => $pb.PbList<Play>();
  @$core.pragma('dart2js:noInline')
  static Play getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Play>(create);
  static Play _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get target => $_getSZ(0);
  @$pb.TagNumber(1)
  set target($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTarget() => $_has(0);
  @$pb.TagNumber(1)
  void clearTarget() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get source => $_getSZ(1);
  @$pb.TagNumber(2)
  set source($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSource() => $_has(1);
  @$pb.TagNumber(2)
  void clearSource() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get package => $_getSZ(2);
  @$pb.TagNumber(3)
  set package($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPackage() => $_has(2);
  @$pb.TagNumber(3)
  void clearPackage() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get type => $_getSZ(3);
  @$pb.TagNumber(4)
  set type($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasType() => $_has(3);
  @$pb.TagNumber(4)
  void clearType() => clearField(4);

  @$pb.TagNumber(5)
  Monetize get monetize => $_getN(4);
  @$pb.TagNumber(5)
  set monetize(Monetize v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasMonetize() => $_has(4);
  @$pb.TagNumber(5)
  void clearMonetize() => clearField(5);
}

