///
//  Generated code. Do not modify.
//  source: CDEService.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'CDEService.pb.dart' as $0;
export 'CDEService.pb.dart';

class CDEServiceClient extends $grpc.Client {
  static final _$searchStream = $grpc.ClientMethod<$0.SearchQuery, $0.Content>(
      '/cloudwalker.CDEService/SearchStream',
      ($0.SearchQuery value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Content.fromBuffer(value));
  static final _$search = $grpc.ClientMethod<$0.SearchQuery, $0.SearchResponse>(
      '/cloudwalker.CDEService/Search',
      ($0.SearchQuery value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.SearchResponse.fromBuffer(value));
  static final _$searchText =
      $grpc.ClientMethod<$0.SearchQuery, $0.SearchTextResponse>(
          '/cloudwalker.CDEService/SearchText',
          ($0.SearchQuery value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.SearchTextResponse.fromBuffer(value));

  CDEServiceClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseStream<$0.Content> searchStream($0.SearchQuery request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$searchStream, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseStream(call);
  }

  $grpc.ResponseFuture<$0.SearchResponse> search($0.SearchQuery request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$search, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.SearchTextResponse> searchText($0.SearchQuery request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$searchText, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class CDEServiceBase extends $grpc.Service {
  $core.String get $name => 'cloudwalker.CDEService';

  CDEServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.SearchQuery, $0.Content>(
        'SearchStream',
        searchStream_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.SearchQuery.fromBuffer(value),
        ($0.Content value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SearchQuery, $0.SearchResponse>(
        'Search',
        search_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SearchQuery.fromBuffer(value),
        ($0.SearchResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SearchQuery, $0.SearchTextResponse>(
        'SearchText',
        searchText_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SearchQuery.fromBuffer(value),
        ($0.SearchTextResponse value) => value.writeToBuffer()));
  }

  $async.Stream<$0.Content> searchStream_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SearchQuery> request) async* {
    yield* searchStream(call, await request);
  }

  $async.Future<$0.SearchResponse> search_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SearchQuery> request) async {
    return search(call, await request);
  }

  $async.Future<$0.SearchTextResponse> searchText_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SearchQuery> request) async {
    return searchText(call, await request);
  }

  $async.Stream<$0.Content> searchStream(
      $grpc.ServiceCall call, $0.SearchQuery request);
  $async.Future<$0.SearchResponse> search(
      $grpc.ServiceCall call, $0.SearchQuery request);
  $async.Future<$0.SearchTextResponse> searchText(
      $grpc.ServiceCall call, $0.SearchQuery request);
}
