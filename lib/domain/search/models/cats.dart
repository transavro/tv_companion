// To parse this JSON data, do
//
//     final cats = catsFromMap(jsonString);

import 'dart:convert';

class Cats {
  Cats({
    this.rowCount,
    this.rows,
  });

  int rowCount;
  List<ContentRow> rows;

  factory Cats.fromJson(String str) => Cats.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Cats.fromMap(Map<String, dynamic> json) => Cats(
        rowCount: json["rowCount"],
        rows: List<ContentRow>.from(
            json["rows"].map((x) => ContentRow.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "rowCount": rowCount,
        "rows": List<dynamic>.from(rows.map((x) => x.toMap())),
      };
}

class ContentRow {
  ContentRow({
    this.rowIndex,
    this.rowHeader,
    this.rowLayout,
    this.rowItems,
  });

  int rowIndex;
  String rowHeader;
  RowLayout rowLayout;
  List<RowItem> rowItems;

  factory ContentRow.fromJson(String str) =>
      ContentRow.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ContentRow.fromMap(Map<String, dynamic> json) => ContentRow(
        rowIndex: json["rowIndex"],
        rowHeader: json["rowHeader"],
        rowLayout: rowLayoutValues.map[json["rowLayout"]],
        rowItems:
            List<RowItem>.from(json["rowItems"].map((x) => RowItem.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "rowIndex": rowIndex,
        "rowHeader": rowHeader,
        "rowLayout": rowLayoutValues.reverse[rowLayout],
        "rowItems": List<dynamic>.from(rowItems.map((x) => x.toMap())),
      };
}

class RowItem {
  RowItem({
    this.tileType,
    this.tid,
    this.title,
    this.poster,
    this.portrait,
    this.background,
    this.rating,
    this.runtime,
    this.startTime,
    this.startIndex,
    this.target,
    this.type,
    this.useAlternate,
    this.alternateUrl,
    this.playstoreUrl,
    this.package,
    this.detailPage,
    this.source,
    this.genre,
    this.year,
    this.director,
    this.cast,
    this.synopsis,
    this.showTileInfo,
    this.tileWidth,
    this.tileHeight,
  });

  TileType tileType;
  String tid;
  String title;
  String poster;
  String portrait;
  String background;
  double rating;
  String runtime;
  int startTime;
  dynamic startIndex;
  List<String> target;
  Type type;
  bool useAlternate;
  String alternateUrl;
  String playstoreUrl;
  Package package;
  bool detailPage;
  Source source;
  List<String> genre;
  String year;
  List<String> director;
  List<String> cast;
  String synopsis;
  String showTileInfo;
  String tileWidth;
  String tileHeight;

  factory RowItem.fromJson(String str) => RowItem.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory RowItem.fromMap(Map<String, dynamic> json) => RowItem(
        tileType: tileTypeValues.map[json["tileType"]],
        tid: json["tid"],
        title: json["title"],
        poster: json["poster"],
        portrait: json["portrait"],
        background: json["background"],
        rating: json["rating"].toDouble(),
        runtime: json["runtime"],
        startTime: json["startTime"],
        startIndex: json["startIndex"],
        target: List<String>.from(json["target"].map((x) => x)),
        type: typeValues.map[json["type"]],
        useAlternate: json["useAlternate"],
        alternateUrl: json["alternateUrl"],
        playstoreUrl:
            json["playstoreUrl"] == null ? null : json["playstoreUrl"],
        package: packageValues.map[json["package"]],
        detailPage: json["detailPage"],
        source: sourceValues.map[json["source"]],
        genre: List<String>.from(json["genre"].map((x) => x)),
        year: json["year"] == null ? null : json["year"],
        director: json["director"] == null
            ? null
            : List<String>.from(json["director"].map((x) => x)),
        cast: json["cast"] == null
            ? null
            : List<String>.from(json["cast"].map((x) => x)),
        synopsis: json["synopsis"] == null ? null : json["synopsis"],
        showTileInfo:
            json["showTileInfo"] == null ? null : json["showTileInfo"],
        tileWidth: json["tileWidth"] == null ? null : json["tileWidth"],
        tileHeight: json["tileHeight"] == null ? null : json["tileHeight"],
      );

  Map<String, dynamic> toMap() => {
        "tileType": tileTypeValues.reverse[tileType],
        "tid": tid,
        "title": title,
        "poster": poster,
        "portrait": portrait,
        "background": background,
        "rating": rating,
        "runtime": runtime,
        "startTime": startTime,
        "startIndex": startIndex,
        "target": List<dynamic>.from(target.map((x) => x)),
        "type": typeValues.reverse[type],
        "useAlternate": useAlternate,
        "alternateUrl": alternateUrl,
        "playstoreUrl": playstoreUrl == null ? null : playstoreUrl,
        "package": packageValues.reverse[package],
        "detailPage": detailPage,
        "source": sourceValues.reverse[source],
        "genre": List<dynamic>.from(genre.map((x) => x)),
        "year": year == null ? null : year,
        "director": director == null
            ? null
            : List<dynamic>.from(director.map((x) => x)),
        "cast": cast == null ? null : List<dynamic>.from(cast.map((x) => x)),
        "synopsis": synopsis == null ? null : synopsis,
        "showTileInfo": showTileInfo == null ? null : showTileInfo,
        "tileWidth": tileWidth == null ? null : tileWidth,
        "tileHeight": tileHeight == null ? null : tileHeight,
      };
}

enum Package {
  COM_GOOGLE_ANDROID_YOUTUBE,
  COM_NETFLIX_MEDIACLIENT,
  IN_STARTV_HOTSTAR,
  COM_EROS_NOW,
  COM_HUNGAMA_MOVIES_TV,
  COM_ZEE5_AOSP,
  COM_VIACOM18_TV_VOOT,
  COM_SUNTV_SUNNXT,
  COM_JIO_MEDIA_STB_ONDEMAND,
  COM_TV_SONYLIV,
  COM_BALAJI_ALT_PARTNER,
  TV_GEMPLEX_FIRETV,
  TV_CLOUDWALKER_PLAYER,
  COM_EPICCHANNEL_EPICON_CLOUDWALKER_TV,
  COM_CLOUDWALKER_SHEMAROOTV,
  COM_YUPPTV_CLOUDWALKER,
  COM_EPIC_DOCUBAY_FIRE,
  TV_CLOUDWALKER_CLOUDWALKERUNIVERSE,
  COM_FLICKSTREE_TV,
  NULL,
  COM_SPOTIFY_TV_ANDROID
}

final packageValues = EnumValues({
  "com.balaji.alt.partner": Package.COM_BALAJI_ALT_PARTNER,
  "com.cloudwalker.shemarootv": Package.COM_CLOUDWALKER_SHEMAROOTV,
  "com.epicchannel.epicon.cloudwalkerTv":
      Package.COM_EPICCHANNEL_EPICON_CLOUDWALKER_TV,
  "com.epic.docubay.fire": Package.COM_EPIC_DOCUBAY_FIRE,
  "com.eros.now": Package.COM_EROS_NOW,
  "com.flickstree.tv": Package.COM_FLICKSTREE_TV,
  "com.google.android.youtube": Package.COM_GOOGLE_ANDROID_YOUTUBE,
  "com.hungama.movies.tv": Package.COM_HUNGAMA_MOVIES_TV,
  "com.jio.media.stb.ondemand": Package.COM_JIO_MEDIA_STB_ONDEMAND,
  "com.netflix.mediaclient": Package.COM_NETFLIX_MEDIACLIENT,
  "com.spotify.tv.android": Package.COM_SPOTIFY_TV_ANDROID,
  "com.suntv.sunnxt": Package.COM_SUNTV_SUNNXT,
  "com.tv.sonyliv": Package.COM_TV_SONYLIV,
  "com.viacom18.tv.voot": Package.COM_VIACOM18_TV_VOOT,
  "com.yupptv.cloudwalker": Package.COM_YUPPTV_CLOUDWALKER,
  "com.zee5.aosp": Package.COM_ZEE5_AOSP,
  "in.startv.hotstar": Package.IN_STARTV_HOTSTAR,
  "null": Package.NULL,
  "tv.cloudwalker.cloudwalkeruniverse":
      Package.TV_CLOUDWALKER_CLOUDWALKERUNIVERSE,
  "tv.cloudwalker.player": Package.TV_CLOUDWALKER_PLAYER,
  "tv.gemplex.firetv": Package.TV_GEMPLEX_FIRETV
});

enum Source {
  YOUTUBE,
  NETFLIX,
  HOTSTAR,
  EROS_NOW,
  HUNGAMA_PLAY,
  ZEE5,
  VOOT,
  SUN_NXT,
  JIO_CINEMA,
  SONY_LIV,
  ALT_BALAJI,
  GEMPLEX,
  CLOUD_TV_STREAMING_PLAYER,
  EPICON,
  HUNGAMA,
  SHEMAROO_ME,
  YUPP_TV,
  DOCU_BAY,
  MOVIE_BOX,
  EPIC_ON,
  FLICKSTREE,
  CLOUDWALKER,
  SPOTIFY
}

final sourceValues = EnumValues({
  "ALTBalaji": Source.ALT_BALAJI,
  "cloudwalker": Source.CLOUDWALKER,
  "Cloud TV Streaming Player": Source.CLOUD_TV_STREAMING_PLAYER,
  "DocuBay": Source.DOCU_BAY,
  "Epicon": Source.EPICON,
  "EpicOn": Source.EPIC_ON,
  "Eros Now": Source.EROS_NOW,
  "Flickstree": Source.FLICKSTREE,
  "Gemplex": Source.GEMPLEX,
  "Hotstar": Source.HOTSTAR,
  "Hungama": Source.HUNGAMA,
  "Hungama Play": Source.HUNGAMA_PLAY,
  "JioCinema": Source.JIO_CINEMA,
  "Movie Box": Source.MOVIE_BOX,
  "Netflix": Source.NETFLIX,
  "ShemarooMe": Source.SHEMAROO_ME,
  "Sony LIV": Source.SONY_LIV,
  "Spotify": Source.SPOTIFY,
  "Sun NXT": Source.SUN_NXT,
  "Voot": Source.VOOT,
  "Youtube": Source.YOUTUBE,
  "YuppTV": Source.YUPP_TV,
  "ZEE5": Source.ZEE5
});

enum TileType { TYPE1, MOVIE }

final tileTypeValues =
    EnumValues({"Movie": TileType.MOVIE, "type1": TileType.TYPE1});

enum Type { CWYT_VIDEO, START, CWYT_PLAYLIST, OPEN_CHANNEL, KEYCODE }

final typeValues = EnumValues({
  "CWYT_PLAYLIST": Type.CWYT_PLAYLIST,
  "CWYT_VIDEO": Type.CWYT_VIDEO,
  "KEYCODE": Type.KEYCODE,
  "OPEN_CHANNEL": Type.OPEN_CHANNEL,
  "START": Type.START
});

enum RowLayout { LANDSCAPE, PORTRAIT, SQUARE }

final rowLayoutValues = EnumValues({
  "landscape": RowLayout.LANDSCAPE,
  "portrait": RowLayout.PORTRAIT,
  "square": RowLayout.SQUARE
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
