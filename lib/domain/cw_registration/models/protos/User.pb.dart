///
//  Generated code. Do not modify.
//  source: User.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'google/type/date.pb.dart' as $2;
import 'google/protobuf/timestamp.pb.dart' as $3;

import 'User.pbenum.dart';

export 'User.pbenum.dart';

class LinkUserRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LinkUserRequest', package: const $pb.PackageName('Cloudwalker'), createEmptyInstance: create)
    ..aOS(1, 'tvEmac', protoName: 'tvEmac')
    ..hasRequiredFields = false
  ;

  LinkUserRequest._() : super();
  factory LinkUserRequest() => create();
  factory LinkUserRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LinkUserRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  LinkUserRequest clone() => LinkUserRequest()..mergeFromMessage(this);
  LinkUserRequest copyWith(void Function(LinkUserRequest) updates) => super.copyWith((message) => updates(message as LinkUserRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LinkUserRequest create() => LinkUserRequest._();
  LinkUserRequest createEmptyInstance() => create();
  static $pb.PbList<LinkUserRequest> createRepeated() => $pb.PbList<LinkUserRequest>();
  @$core.pragma('dart2js:noInline')
  static LinkUserRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LinkUserRequest>(create);
  static LinkUserRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get tvEmac => $_getSZ(0);
  @$pb.TagNumber(1)
  set tvEmac($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTvEmac() => $_has(0);
  @$pb.TagNumber(1)
  void clearTvEmac() => clearField(1);
}

class LinkUserResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LinkUserResponse', package: const $pb.PackageName('Cloudwalker'), createEmptyInstance: create)
    ..pc<User>(1, 'user', $pb.PbFieldType.PM, subBuilder: User.create)
    ..hasRequiredFields = false
  ;

  LinkUserResponse._() : super();
  factory LinkUserResponse() => create();
  factory LinkUserResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LinkUserResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  LinkUserResponse clone() => LinkUserResponse()..mergeFromMessage(this);
  LinkUserResponse copyWith(void Function(LinkUserResponse) updates) => super.copyWith((message) => updates(message as LinkUserResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LinkUserResponse create() => LinkUserResponse._();
  LinkUserResponse createEmptyInstance() => create();
  static $pb.PbList<LinkUserResponse> createRepeated() => $pb.PbList<LinkUserResponse>();
  @$core.pragma('dart2js:noInline')
  static LinkUserResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LinkUserResponse>(create);
  static LinkUserResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<User> get user => $_getList(0);
}

class RemoveTvDeviceRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('RemoveTvDeviceRequest', package: const $pb.PackageName('Cloudwalker'), createEmptyInstance: create)
    ..aOS(1, 'googleId', protoName: 'googleId')
    ..aOS(2, 'tvEmac', protoName: 'tvEmac')
    ..hasRequiredFields = false
  ;

  RemoveTvDeviceRequest._() : super();
  factory RemoveTvDeviceRequest() => create();
  factory RemoveTvDeviceRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RemoveTvDeviceRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  RemoveTvDeviceRequest clone() => RemoveTvDeviceRequest()..mergeFromMessage(this);
  RemoveTvDeviceRequest copyWith(void Function(RemoveTvDeviceRequest) updates) => super.copyWith((message) => updates(message as RemoveTvDeviceRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RemoveTvDeviceRequest create() => RemoveTvDeviceRequest._();
  RemoveTvDeviceRequest createEmptyInstance() => create();
  static $pb.PbList<RemoveTvDeviceRequest> createRepeated() => $pb.PbList<RemoveTvDeviceRequest>();
  @$core.pragma('dart2js:noInline')
  static RemoveTvDeviceRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RemoveTvDeviceRequest>(create);
  static RemoveTvDeviceRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get googleId => $_getSZ(0);
  @$pb.TagNumber(1)
  set googleId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasGoogleId() => $_has(0);
  @$pb.TagNumber(1)
  void clearGoogleId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get tvEmac => $_getSZ(1);
  @$pb.TagNumber(2)
  set tvEmac($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTvEmac() => $_has(1);
  @$pb.TagNumber(2)
  void clearTvEmac() => clearField(2);
}

class RemoveTvDeviceResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('RemoveTvDeviceResponse', package: const $pb.PackageName('Cloudwalker'), createEmptyInstance: create)
    ..aOB(1, 'isTvDeviceRemoved', protoName: 'isTvDeviceRemoved')
    ..hasRequiredFields = false
  ;

  RemoveTvDeviceResponse._() : super();
  factory RemoveTvDeviceResponse() => create();
  factory RemoveTvDeviceResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RemoveTvDeviceResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  RemoveTvDeviceResponse clone() => RemoveTvDeviceResponse()..mergeFromMessage(this);
  RemoveTvDeviceResponse copyWith(void Function(RemoveTvDeviceResponse) updates) => super.copyWith((message) => updates(message as RemoveTvDeviceResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RemoveTvDeviceResponse create() => RemoveTvDeviceResponse._();
  RemoveTvDeviceResponse createEmptyInstance() => create();
  static $pb.PbList<RemoveTvDeviceResponse> createRepeated() => $pb.PbList<RemoveTvDeviceResponse>();
  @$core.pragma('dart2js:noInline')
  static RemoveTvDeviceResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RemoveTvDeviceResponse>(create);
  static RemoveTvDeviceResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get isTvDeviceRemoved => $_getBF(0);
  @$pb.TagNumber(1)
  set isTvDeviceRemoved($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasIsTvDeviceRemoved() => $_has(0);
  @$pb.TagNumber(1)
  void clearIsTvDeviceRemoved() => clearField(1);
}

class GetRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('GetRequest', package: const $pb.PackageName('Cloudwalker'), createEmptyInstance: create)
    ..aOS(1, 'googleId', protoName: 'googleId')
    ..hasRequiredFields = false
  ;

  GetRequest._() : super();
  factory GetRequest() => create();
  factory GetRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  GetRequest clone() => GetRequest()..mergeFromMessage(this);
  GetRequest copyWith(void Function(GetRequest) updates) => super.copyWith((message) => updates(message as GetRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetRequest create() => GetRequest._();
  GetRequest createEmptyInstance() => create();
  static $pb.PbList<GetRequest> createRepeated() => $pb.PbList<GetRequest>();
  @$core.pragma('dart2js:noInline')
  static GetRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetRequest>(create);
  static GetRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get googleId => $_getSZ(0);
  @$pb.TagNumber(1)
  set googleId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasGoogleId() => $_has(0);
  @$pb.TagNumber(1)
  void clearGoogleId() => clearField(1);
}

class DeleteRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('DeleteRequest', package: const $pb.PackageName('Cloudwalker'), createEmptyInstance: create)
    ..aOS(1, 'googleId', protoName: 'googleId')
    ..hasRequiredFields = false
  ;

  DeleteRequest._() : super();
  factory DeleteRequest() => create();
  factory DeleteRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DeleteRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  DeleteRequest clone() => DeleteRequest()..mergeFromMessage(this);
  DeleteRequest copyWith(void Function(DeleteRequest) updates) => super.copyWith((message) => updates(message as DeleteRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DeleteRequest create() => DeleteRequest._();
  DeleteRequest createEmptyInstance() => create();
  static $pb.PbList<DeleteRequest> createRepeated() => $pb.PbList<DeleteRequest>();
  @$core.pragma('dart2js:noInline')
  static DeleteRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DeleteRequest>(create);
  static DeleteRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get googleId => $_getSZ(0);
  @$pb.TagNumber(1)
  set googleId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasGoogleId() => $_has(0);
  @$pb.TagNumber(1)
  void clearGoogleId() => clearField(1);
}

class DeleteReponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('DeleteReponse', package: const $pb.PackageName('Cloudwalker'), createEmptyInstance: create)
    ..aOB(1, 'isDeleted', protoName: 'isDeleted')
    ..hasRequiredFields = false
  ;

  DeleteReponse._() : super();
  factory DeleteReponse() => create();
  factory DeleteReponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DeleteReponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  DeleteReponse clone() => DeleteReponse()..mergeFromMessage(this);
  DeleteReponse copyWith(void Function(DeleteReponse) updates) => super.copyWith((message) => updates(message as DeleteReponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DeleteReponse create() => DeleteReponse._();
  DeleteReponse createEmptyInstance() => create();
  static $pb.PbList<DeleteReponse> createRepeated() => $pb.PbList<DeleteReponse>();
  @$core.pragma('dart2js:noInline')
  static DeleteReponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DeleteReponse>(create);
  static DeleteReponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get isDeleted => $_getBF(0);
  @$pb.TagNumber(1)
  set isDeleted($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasIsDeleted() => $_has(0);
  @$pb.TagNumber(1)
  void clearIsDeleted() => clearField(1);
}

class LinkedDeviceResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LinkedDeviceResponse', package: const $pb.PackageName('Cloudwalker'), createEmptyInstance: create)
    ..aOB(1, 'isLinkedDeviceFetched', protoName: 'isLinkedDeviceFetched')
    ..pc<LinkedDevice>(2, 'linkedDevices', $pb.PbFieldType.PM, protoName: 'linkedDevices', subBuilder: LinkedDevice.create)
    ..hasRequiredFields = false
  ;

  LinkedDeviceResponse._() : super();
  factory LinkedDeviceResponse() => create();
  factory LinkedDeviceResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LinkedDeviceResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  LinkedDeviceResponse clone() => LinkedDeviceResponse()..mergeFromMessage(this);
  LinkedDeviceResponse copyWith(void Function(LinkedDeviceResponse) updates) => super.copyWith((message) => updates(message as LinkedDeviceResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LinkedDeviceResponse create() => LinkedDeviceResponse._();
  LinkedDeviceResponse createEmptyInstance() => create();
  static $pb.PbList<LinkedDeviceResponse> createRepeated() => $pb.PbList<LinkedDeviceResponse>();
  @$core.pragma('dart2js:noInline')
  static LinkedDeviceResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LinkedDeviceResponse>(create);
  static LinkedDeviceResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get isLinkedDeviceFetched => $_getBF(0);
  @$pb.TagNumber(1)
  set isLinkedDeviceFetched($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasIsLinkedDeviceFetched() => $_has(0);
  @$pb.TagNumber(1)
  void clearIsLinkedDeviceFetched() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<LinkedDevice> get linkedDevices => $_getList(1);
}

class TvDevice extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('TvDevice', package: const $pb.PackageName('Cloudwalker'), createEmptyInstance: create)
    ..aOS(1, 'googleId', protoName: 'googleId')
    ..aOM<LinkedDevice>(2, 'linkedDevice', protoName: 'linkedDevice', subBuilder: LinkedDevice.create)
    ..hasRequiredFields = false
  ;

  TvDevice._() : super();
  factory TvDevice() => create();
  factory TvDevice.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TvDevice.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  TvDevice clone() => TvDevice()..mergeFromMessage(this);
  TvDevice copyWith(void Function(TvDevice) updates) => super.copyWith((message) => updates(message as TvDevice));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TvDevice create() => TvDevice._();
  TvDevice createEmptyInstance() => create();
  static $pb.PbList<TvDevice> createRepeated() => $pb.PbList<TvDevice>();
  @$core.pragma('dart2js:noInline')
  static TvDevice getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TvDevice>(create);
  static TvDevice _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get googleId => $_getSZ(0);
  @$pb.TagNumber(1)
  set googleId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasGoogleId() => $_has(0);
  @$pb.TagNumber(1)
  void clearGoogleId() => clearField(1);

  @$pb.TagNumber(2)
  LinkedDevice get linkedDevice => $_getN(1);
  @$pb.TagNumber(2)
  set linkedDevice(LinkedDevice v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasLinkedDevice() => $_has(1);
  @$pb.TagNumber(2)
  void clearLinkedDevice() => clearField(2);
  @$pb.TagNumber(2)
  LinkedDevice ensureLinkedDevice() => $_ensure(1);
}

class UpdateResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('UpdateResponse', package: const $pb.PackageName('Cloudwalker'), createEmptyInstance: create)
    ..aOB(1, 'isUpdated', protoName: 'isUpdated')
    ..hasRequiredFields = false
  ;

  UpdateResponse._() : super();
  factory UpdateResponse() => create();
  factory UpdateResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UpdateResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  UpdateResponse clone() => UpdateResponse()..mergeFromMessage(this);
  UpdateResponse copyWith(void Function(UpdateResponse) updates) => super.copyWith((message) => updates(message as UpdateResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UpdateResponse create() => UpdateResponse._();
  UpdateResponse createEmptyInstance() => create();
  static $pb.PbList<UpdateResponse> createRepeated() => $pb.PbList<UpdateResponse>();
  @$core.pragma('dart2js:noInline')
  static UpdateResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UpdateResponse>(create);
  static UpdateResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get isUpdated => $_getBF(0);
  @$pb.TagNumber(1)
  set isUpdated($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasIsUpdated() => $_has(0);
  @$pb.TagNumber(1)
  void clearIsUpdated() => clearField(1);
}

class CreateReponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('CreateReponse', package: const $pb.PackageName('Cloudwalker'), createEmptyInstance: create)
    ..aOB(1, 'isCreated', protoName: 'isCreated')
    ..hasRequiredFields = false
  ;

  CreateReponse._() : super();
  factory CreateReponse() => create();
  factory CreateReponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CreateReponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  CreateReponse clone() => CreateReponse()..mergeFromMessage(this);
  CreateReponse copyWith(void Function(CreateReponse) updates) => super.copyWith((message) => updates(message as CreateReponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CreateReponse create() => CreateReponse._();
  CreateReponse createEmptyInstance() => create();
  static $pb.PbList<CreateReponse> createRepeated() => $pb.PbList<CreateReponse>();
  @$core.pragma('dart2js:noInline')
  static CreateReponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CreateReponse>(create);
  static CreateReponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get isCreated => $_getBF(0);
  @$pb.TagNumber(1)
  set isCreated($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasIsCreated() => $_has(0);
  @$pb.TagNumber(1)
  void clearIsCreated() => clearField(1);
}

class User extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('User', package: const $pb.PackageName('Cloudwalker'), createEmptyInstance: create)
    ..aOS(1, 'name')
    ..aOS(2, 'email')
    ..aOS(3, 'phoneNumber', protoName: 'phoneNumber')
    ..aOS(4, 'googleId', protoName: 'googleId')
    ..aOM<$2.Date>(5, 'dateOfBirth', protoName: 'dateOfBirth', subBuilder: $2.Date.create)
    ..pPS(6, 'genre')
    ..pPS(7, 'language')
    ..pPS(8, 'contentType', protoName: 'contentType')
    ..pc<LinkedDevice>(9, 'linkedDevices', $pb.PbFieldType.PM, protoName: 'linkedDevices', subBuilder: LinkedDevice.create)
    ..aOM<$3.Timestamp>(10, 'createdAt', protoName: 'createdAt', subBuilder: $3.Timestamp.create)
    ..aOM<$3.Timestamp>(11, 'updatedAt', protoName: 'updatedAt', subBuilder: $3.Timestamp.create)
    ..e<Gender>(12, 'gender', $pb.PbFieldType.OE, defaultOrMaker: Gender.NOTDEFINED, valueOf: Gender.valueOf, enumValues: Gender.values)
    ..aOS(13, 'imageUrl', protoName: 'imageUrl')
    ..hasRequiredFields = false
  ;

  User._() : super();
  factory User() => create();
  factory User.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory User.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  User clone() => User()..mergeFromMessage(this);
  User copyWith(void Function(User) updates) => super.copyWith((message) => updates(message as User));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static User create() => User._();
  User createEmptyInstance() => create();
  static $pb.PbList<User> createRepeated() => $pb.PbList<User>();
  @$core.pragma('dart2js:noInline')
  static User getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<User>(create);
  static User _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get email => $_getSZ(1);
  @$pb.TagNumber(2)
  set email($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasEmail() => $_has(1);
  @$pb.TagNumber(2)
  void clearEmail() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get phoneNumber => $_getSZ(2);
  @$pb.TagNumber(3)
  set phoneNumber($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPhoneNumber() => $_has(2);
  @$pb.TagNumber(3)
  void clearPhoneNumber() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get googleId => $_getSZ(3);
  @$pb.TagNumber(4)
  set googleId($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasGoogleId() => $_has(3);
  @$pb.TagNumber(4)
  void clearGoogleId() => clearField(4);

  @$pb.TagNumber(5)
  $2.Date get dateOfBirth => $_getN(4);
  @$pb.TagNumber(5)
  set dateOfBirth($2.Date v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasDateOfBirth() => $_has(4);
  @$pb.TagNumber(5)
  void clearDateOfBirth() => clearField(5);
  @$pb.TagNumber(5)
  $2.Date ensureDateOfBirth() => $_ensure(4);

  @$pb.TagNumber(6)
  $core.List<$core.String> get genre => $_getList(5);

  @$pb.TagNumber(7)
  $core.List<$core.String> get language => $_getList(6);

  @$pb.TagNumber(8)
  $core.List<$core.String> get contentType => $_getList(7);

  @$pb.TagNumber(9)
  $core.List<LinkedDevice> get linkedDevices => $_getList(8);

  @$pb.TagNumber(10)
  $3.Timestamp get createdAt => $_getN(9);
  @$pb.TagNumber(10)
  set createdAt($3.Timestamp v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasCreatedAt() => $_has(9);
  @$pb.TagNumber(10)
  void clearCreatedAt() => clearField(10);
  @$pb.TagNumber(10)
  $3.Timestamp ensureCreatedAt() => $_ensure(9);

  @$pb.TagNumber(11)
  $3.Timestamp get updatedAt => $_getN(10);
  @$pb.TagNumber(11)
  set updatedAt($3.Timestamp v) { setField(11, v); }
  @$pb.TagNumber(11)
  $core.bool hasUpdatedAt() => $_has(10);
  @$pb.TagNumber(11)
  void clearUpdatedAt() => clearField(11);
  @$pb.TagNumber(11)
  $3.Timestamp ensureUpdatedAt() => $_ensure(10);

  @$pb.TagNumber(12)
  Gender get gender => $_getN(11);
  @$pb.TagNumber(12)
  set gender(Gender v) { setField(12, v); }
  @$pb.TagNumber(12)
  $core.bool hasGender() => $_has(11);
  @$pb.TagNumber(12)
  void clearGender() => clearField(12);

  @$pb.TagNumber(13)
  $core.String get imageUrl => $_getSZ(12);
  @$pb.TagNumber(13)
  set imageUrl($core.String v) { $_setString(12, v); }
  @$pb.TagNumber(13)
  $core.bool hasImageUrl() => $_has(12);
  @$pb.TagNumber(13)
  void clearImageUrl() => clearField(13);
}

class LinkedDevice extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LinkedDevice', package: const $pb.PackageName('Cloudwalker'), createEmptyInstance: create)
    ..aOS(1, 'tvEmac', protoName: 'tvEmac')
    ..aOS(2, 'tvPanel', protoName: 'tvPanel')
    ..aOS(3, 'tvBoard', protoName: 'tvBoard')
    ..aOS(4, 'tvName', protoName: 'tvName')
    ..hasRequiredFields = false
  ;

  LinkedDevice._() : super();
  factory LinkedDevice() => create();
  factory LinkedDevice.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LinkedDevice.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  LinkedDevice clone() => LinkedDevice()..mergeFromMessage(this);
  LinkedDevice copyWith(void Function(LinkedDevice) updates) => super.copyWith((message) => updates(message as LinkedDevice));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LinkedDevice create() => LinkedDevice._();
  LinkedDevice createEmptyInstance() => create();
  static $pb.PbList<LinkedDevice> createRepeated() => $pb.PbList<LinkedDevice>();
  @$core.pragma('dart2js:noInline')
  static LinkedDevice getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LinkedDevice>(create);
  static LinkedDevice _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get tvEmac => $_getSZ(0);
  @$pb.TagNumber(1)
  set tvEmac($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTvEmac() => $_has(0);
  @$pb.TagNumber(1)
  void clearTvEmac() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get tvPanel => $_getSZ(1);
  @$pb.TagNumber(2)
  set tvPanel($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTvPanel() => $_has(1);
  @$pb.TagNumber(2)
  void clearTvPanel() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get tvBoard => $_getSZ(2);
  @$pb.TagNumber(3)
  set tvBoard($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTvBoard() => $_has(2);
  @$pb.TagNumber(3)
  void clearTvBoard() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get tvName => $_getSZ(3);
  @$pb.TagNumber(4)
  set tvName($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasTvName() => $_has(3);
  @$pb.TagNumber(4)
  void clearTvName() => clearField(4);
}

