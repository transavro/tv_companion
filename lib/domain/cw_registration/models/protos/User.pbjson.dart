///
//  Generated code. Do not modify.
//  source: User.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const Gender$json = const {
  '1': 'Gender',
  '2': const [
    const {'1': 'NOTDEFINED', '2': 0},
    const {'1': 'MALE', '2': 1},
    const {'1': 'FEMALE', '2': 2},
  ],
};

const LinkUserRequest$json = const {
  '1': 'LinkUserRequest',
  '2': const [
    const {'1': 'tvEmac', '3': 1, '4': 1, '5': 9, '10': 'tvEmac'},
  ],
};

const LinkUserResponse$json = const {
  '1': 'LinkUserResponse',
  '2': const [
    const {'1': 'user', '3': 1, '4': 3, '5': 11, '6': '.Cloudwalker.User', '10': 'user'},
  ],
};

const RemoveTvDeviceRequest$json = const {
  '1': 'RemoveTvDeviceRequest',
  '2': const [
    const {'1': 'googleId', '3': 1, '4': 1, '5': 9, '10': 'googleId'},
    const {'1': 'tvEmac', '3': 2, '4': 1, '5': 9, '10': 'tvEmac'},
  ],
};

const RemoveTvDeviceResponse$json = const {
  '1': 'RemoveTvDeviceResponse',
  '2': const [
    const {'1': 'isTvDeviceRemoved', '3': 1, '4': 1, '5': 8, '10': 'isTvDeviceRemoved'},
  ],
};

const GetRequest$json = const {
  '1': 'GetRequest',
  '2': const [
    const {'1': 'googleId', '3': 1, '4': 1, '5': 9, '10': 'googleId'},
  ],
};

const DeleteRequest$json = const {
  '1': 'DeleteRequest',
  '2': const [
    const {'1': 'googleId', '3': 1, '4': 1, '5': 9, '10': 'googleId'},
  ],
};

const DeleteReponse$json = const {
  '1': 'DeleteReponse',
  '2': const [
    const {'1': 'isDeleted', '3': 1, '4': 1, '5': 8, '10': 'isDeleted'},
  ],
};

const LinkedDeviceResponse$json = const {
  '1': 'LinkedDeviceResponse',
  '2': const [
    const {'1': 'isLinkedDeviceFetched', '3': 1, '4': 1, '5': 8, '10': 'isLinkedDeviceFetched'},
    const {'1': 'linkedDevices', '3': 2, '4': 3, '5': 11, '6': '.Cloudwalker.LinkedDevice', '10': 'linkedDevices'},
  ],
};

const TvDevice$json = const {
  '1': 'TvDevice',
  '2': const [
    const {'1': 'googleId', '3': 1, '4': 1, '5': 9, '10': 'googleId'},
    const {'1': 'linkedDevice', '3': 2, '4': 1, '5': 11, '6': '.Cloudwalker.LinkedDevice', '10': 'linkedDevice'},
  ],
};

const UpdateResponse$json = const {
  '1': 'UpdateResponse',
  '2': const [
    const {'1': 'isUpdated', '3': 1, '4': 1, '5': 8, '10': 'isUpdated'},
  ],
};

const CreateReponse$json = const {
  '1': 'CreateReponse',
  '2': const [
    const {'1': 'isCreated', '3': 1, '4': 1, '5': 8, '10': 'isCreated'},
  ],
};

const User$json = const {
  '1': 'User',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'email', '3': 2, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'phoneNumber', '3': 3, '4': 1, '5': 9, '10': 'phoneNumber'},
    const {'1': 'googleId', '3': 4, '4': 1, '5': 9, '10': 'googleId'},
    const {'1': 'dateOfBirth', '3': 5, '4': 1, '5': 11, '6': '.google.type.Date', '10': 'dateOfBirth'},
    const {'1': 'gender', '3': 12, '4': 1, '5': 14, '6': '.Cloudwalker.Gender', '10': 'gender'},
    const {'1': 'imageUrl', '3': 13, '4': 1, '5': 9, '10': 'imageUrl'},
    const {'1': 'genre', '3': 6, '4': 3, '5': 9, '10': 'genre'},
    const {'1': 'language', '3': 7, '4': 3, '5': 9, '10': 'language'},
    const {'1': 'contentType', '3': 8, '4': 3, '5': 9, '10': 'contentType'},
    const {'1': 'linkedDevices', '3': 9, '4': 3, '5': 11, '6': '.Cloudwalker.LinkedDevice', '10': 'linkedDevices'},
    const {'1': 'createdAt', '3': 10, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'createdAt'},
    const {'1': 'updatedAt', '3': 11, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'updatedAt'},
  ],
};

const LinkedDevice$json = const {
  '1': 'LinkedDevice',
  '2': const [
    const {'1': 'tvEmac', '3': 1, '4': 1, '5': 9, '10': 'tvEmac'},
    const {'1': 'tvPanel', '3': 2, '4': 1, '5': 9, '10': 'tvPanel'},
    const {'1': 'tvBoard', '3': 3, '4': 1, '5': 9, '10': 'tvBoard'},
    const {'1': 'tvName', '3': 4, '4': 1, '5': 9, '10': 'tvName'},
  ],
};

