///
//  Generated code. Do not modify.
//  source: User.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'User.pb.dart' as $0;
import 'google/protobuf/empty.pb.dart' as $1;
export 'User.pb.dart';

class UserServiceClient extends $grpc.Client {
  static final _$addUser = $grpc.ClientMethod<$0.User, $1.Empty>(
      '/Cloudwalker.UserService/AddUser',
      ($0.User value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));
  static final _$listUsers = $grpc.ClientMethod<$1.Empty, $0.User>(
      '/Cloudwalker.UserService/ListUsers',
      ($1.Empty value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.User.fromBuffer(value));
  static final _$getUser = $grpc.ClientMethod<$0.GetRequest, $0.User>(
      '/Cloudwalker.UserService/GetUser',
      ($0.GetRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.User.fromBuffer(value));
  static final _$updateUser = $grpc.ClientMethod<$0.User, $0.User>(
      '/Cloudwalker.UserService/UpdateUser',
      ($0.User value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.User.fromBuffer(value));
  static final _$deleteUser =
      $grpc.ClientMethod<$0.DeleteRequest, $0.DeleteReponse>(
          '/Cloudwalker.UserService/DeleteUser',
          ($0.DeleteRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.DeleteReponse.fromBuffer(value));
  static final _$addTvDevice =
      $grpc.ClientMethod<$0.TvDevice, $0.LinkedDeviceResponse>(
          '/Cloudwalker.UserService/AddTvDevice',
          ($0.TvDevice value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.LinkedDeviceResponse.fromBuffer(value));
  static final _$removeTvDevice =
      $grpc.ClientMethod<$0.RemoveTvDeviceRequest, $0.RemoveTvDeviceResponse>(
          '/Cloudwalker.UserService/RemoveTvDevice',
          ($0.RemoveTvDeviceRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.RemoveTvDeviceResponse.fromBuffer(value));
  static final _$getLinkedDevices =
      $grpc.ClientMethod<$0.GetRequest, $0.LinkedDeviceResponse>(
          '/Cloudwalker.UserService/GetLinkedDevices',
          ($0.GetRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.LinkedDeviceResponse.fromBuffer(value));
  static final _$getLinkedUsers =
      $grpc.ClientMethod<$0.LinkUserRequest, $0.LinkUserResponse>(
          '/Cloudwalker.UserService/GetLinkedUsers',
          ($0.LinkUserRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.LinkUserResponse.fromBuffer(value));

  UserServiceClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$1.Empty> addUser($0.User request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$addUser, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseStream<$0.User> listUsers($1.Empty request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$listUsers, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseStream(call);
  }

  $grpc.ResponseFuture<$0.User> getUser($0.GetRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$getUser, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.User> updateUser($0.User request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$updateUser, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.DeleteReponse> deleteUser($0.DeleteRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$deleteUser, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.LinkedDeviceResponse> addTvDevice($0.TvDevice request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$addTvDevice, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.RemoveTvDeviceResponse> removeTvDevice(
      $0.RemoveTvDeviceRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$removeTvDevice, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.LinkedDeviceResponse> getLinkedDevices(
      $0.GetRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$getLinkedDevices, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.LinkUserResponse> getLinkedUsers(
      $0.LinkUserRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$getLinkedUsers, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class UserServiceBase extends $grpc.Service {
  $core.String get $name => 'Cloudwalker.UserService';

  UserServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.User, $1.Empty>(
        'AddUser',
        addUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.User.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.Empty, $0.User>(
        'ListUsers',
        listUsers_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $1.Empty.fromBuffer(value),
        ($0.User value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetRequest, $0.User>(
        'GetUser',
        getUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.GetRequest.fromBuffer(value),
        ($0.User value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.User, $0.User>(
        'UpdateUser',
        updateUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.User.fromBuffer(value),
        ($0.User value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DeleteRequest, $0.DeleteReponse>(
        'DeleteUser',
        deleteUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.DeleteRequest.fromBuffer(value),
        ($0.DeleteReponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TvDevice, $0.LinkedDeviceResponse>(
        'AddTvDevice',
        addTvDevice_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TvDevice.fromBuffer(value),
        ($0.LinkedDeviceResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RemoveTvDeviceRequest,
            $0.RemoveTvDeviceResponse>(
        'RemoveTvDevice',
        removeTvDevice_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.RemoveTvDeviceRequest.fromBuffer(value),
        ($0.RemoveTvDeviceResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetRequest, $0.LinkedDeviceResponse>(
        'GetLinkedDevices',
        getLinkedDevices_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.GetRequest.fromBuffer(value),
        ($0.LinkedDeviceResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LinkUserRequest, $0.LinkUserResponse>(
        'GetLinkedUsers',
        getLinkedUsers_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.LinkUserRequest.fromBuffer(value),
        ($0.LinkUserResponse value) => value.writeToBuffer()));
  }

  $async.Future<$1.Empty> addUser_Pre(
      $grpc.ServiceCall call, $async.Future<$0.User> request) async {
    return addUser(call, await request);
  }

  $async.Stream<$0.User> listUsers_Pre(
      $grpc.ServiceCall call, $async.Future<$1.Empty> request) async* {
    yield* listUsers(call, await request);
  }

  $async.Future<$0.User> getUser_Pre(
      $grpc.ServiceCall call, $async.Future<$0.GetRequest> request) async {
    return getUser(call, await request);
  }

  $async.Future<$0.User> updateUser_Pre(
      $grpc.ServiceCall call, $async.Future<$0.User> request) async {
    return updateUser(call, await request);
  }

  $async.Future<$0.DeleteReponse> deleteUser_Pre(
      $grpc.ServiceCall call, $async.Future<$0.DeleteRequest> request) async {
    return deleteUser(call, await request);
  }

  $async.Future<$0.LinkedDeviceResponse> addTvDevice_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TvDevice> request) async {
    return addTvDevice(call, await request);
  }

  $async.Future<$0.RemoveTvDeviceResponse> removeTvDevice_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.RemoveTvDeviceRequest> request) async {
    return removeTvDevice(call, await request);
  }

  $async.Future<$0.LinkedDeviceResponse> getLinkedDevices_Pre(
      $grpc.ServiceCall call, $async.Future<$0.GetRequest> request) async {
    return getLinkedDevices(call, await request);
  }

  $async.Future<$0.LinkUserResponse> getLinkedUsers_Pre(
      $grpc.ServiceCall call, $async.Future<$0.LinkUserRequest> request) async {
    return getLinkedUsers(call, await request);
  }

  $async.Future<$1.Empty> addUser($grpc.ServiceCall call, $0.User request);
  $async.Stream<$0.User> listUsers($grpc.ServiceCall call, $1.Empty request);
  $async.Future<$0.User> getUser($grpc.ServiceCall call, $0.GetRequest request);
  $async.Future<$0.User> updateUser($grpc.ServiceCall call, $0.User request);
  $async.Future<$0.DeleteReponse> deleteUser(
      $grpc.ServiceCall call, $0.DeleteRequest request);
  $async.Future<$0.LinkedDeviceResponse> addTvDevice(
      $grpc.ServiceCall call, $0.TvDevice request);
  $async.Future<$0.RemoveTvDeviceResponse> removeTvDevice(
      $grpc.ServiceCall call, $0.RemoveTvDeviceRequest request);
  $async.Future<$0.LinkedDeviceResponse> getLinkedDevices(
      $grpc.ServiceCall call, $0.GetRequest request);
  $async.Future<$0.LinkUserResponse> getLinkedUsers(
      $grpc.ServiceCall call, $0.LinkUserRequest request);
}
