import 'package:cloudwalker_companion/domain/google_sign_in/models/user_google_info.dart';

class NoCwSignInException implements Exception{
  final UserGoogleInfo _userGoogleInfo;

  NoCwSignInException(this._userGoogleInfo);
  
  UserGoogleInfo get userGoogleInfo => _userGoogleInfo;
}