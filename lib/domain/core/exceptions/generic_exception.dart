class GenericException implements Exception{
  String _errorMessage;
  GenericException([String message = 'Server Error']) {
    this._errorMessage = message;
  }

  @override
  String toString() {
    return _errorMessage;
  }
}