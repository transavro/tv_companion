class ServerErrorException implements Exception {
  String _errorMessage;
  ServerErrorException([String message = 'Server Error']) {
    this._errorMessage = message;
  }

  @override
  String toString() {
    return _errorMessage;
  }
}
