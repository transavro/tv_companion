import 'package:auto_route/auto_route.dart';
import 'package:cloudwalker_companion/presentation/core/widgets/title.dart';
import 'package:flutter/material.dart';

class NetworkDevices extends StatefulWidget {
  final Map<String, int> avaliableDevices;

  const NetworkDevices({Key key, this.avaliableDevices}) : super(key: key);

  @override
  _NetworkDevicesState createState() => _NetworkDevicesState();
}

class _NetworkDevicesState extends State<NetworkDevices> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        body: _buildDeviceFoundUi(widget.avaliableDevices));
  }
}

Widget _buildDeviceFoundUi(Map<String, int> deviceList) {
  return Column(
    children: [
      SizedBox(
        height: 60,
      ),
      buildTitle("Select TV"),
      SizedBox(
        height: 20,
      ),
      Text(
        "Avaliable devices",
        style: TextStyle(
          fontSize: 10,
          color: Colors.blue[600],
        ),
      ),
      _buildDeviceList(deviceList),
    ],
  );
}

Widget _buildDeviceList(Map<String, int> deviceList) {
  final keySet = deviceList.keys;
  return ListView.separated(
    physics: const AlwaysScrollableScrollPhysics(),
    scrollDirection: Axis.vertical,
    itemCount: deviceList.length,
    shrinkWrap: true,
    itemBuilder: (context, index) {
      final lable = keySet
          .elementAt(index)
          .replaceRange(21, keySet.elementAt(index).length, "");
      return ListTile(
        onTap: () {
          Map<String, int> resultMap = Map();
          resultMap[keySet.elementAt(index)] =
              deviceList[keySet.elementAt(index)];
          ExtendedNavigator.of(context).pop(resultMap);
        },
        leading: Icon(
          Icons.tv,
          color: Colors.white,
        ),
        trailing: Icon(
          Icons.delete,
          color: Colors.white,
        ),
        title: Text(lable,
            style: TextStyle(
              fontSize: 13,
              color: Colors.white,
            )),
      );
    },
    separatorBuilder: (context, index) {
      return Divider(
        color: Colors.grey,
      );
    },
  );
}
