import 'dart:async';
import 'dart:convert';

import 'package:auto_route/auto_route.dart';
import 'package:cloudwalker_companion/domain/google_sign_in/models/user_google_info.dart';
import 'package:cloudwalker_companion/presentation/core/widgets/sub_title.dart';
import 'package:cloudwalker_companion/presentation/core/widgets/title.dart';
import 'package:flushbar/flushbar_helper.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:http/http.dart' as http;
import 'package:cloudwalker_companion/presentation/routes/router.gr.dart';

class PinCodeVerificationScreen extends StatefulWidget {
  final String phoneNumber;
  final UserGoogleInfo userGoogleInfo;

  const PinCodeVerificationScreen(
      {Key key, this.phoneNumber, this.userGoogleInfo})
      : super(key: key);

  @override
  _PinCodeVerificationScreenState createState() =>
      _PinCodeVerificationScreenState();
}

class _PinCodeVerificationScreenState extends State<PinCodeVerificationScreen> {
  var onTapRecognizer;
  String verifySession = "";
  final smsApiKey = "d384caff-5a5e-11e7-94da-0200cd936042";

  TextEditingController textEditingController = TextEditingController()
    ..text = "";

  StreamController<ErrorAnimationType> errorController;

  bool hasError = false;
  String currentText = "";
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();

  @override
  void initState() {
    sendForVerfication();
    onTapRecognizer = TapGestureRecognizer()
      ..onTap = () {
        sendForVerfication();
      };
    errorController = StreamController<ErrorAnimationType>();

    super.initState();
  }

  sendForVerfication() async {
    final response = await http.get(
        'https://2factor.in/API/V1/$smsApiKey/SMS/${widget.phoneNumber}/AUTOGEN');

    if (response.statusCode == 200) {
      Map<String, dynamic> verfiyDetails = jsonDecode(response.body);
      if (verfiyDetails['Status'] == "Success") {
        verifySession = verfiyDetails['Details'];
        FlushbarHelper.createInformation(
                title: "Sending Otp on your phone...",
                message: "Thank you.",
                duration: Duration(seconds: 1))
            .show(context);
      } else {
        FlushbarHelper.createError(
                title: "Error while sending verfiying mobile Number.",
                message: "Please try again.",
                duration: Duration(seconds: 1))
            .show(context);
      }
    } else {
      FlushbarHelper.createError(
              title: "Error while sending verfiying mobile Number.",
              message: "Please try again. ${response.statusCode}",
              duration: Duration(seconds: 1))
          .show(context);
    }
  }

  verifyOTP(String otp) async {
    final response = await http.get(
        'https://2factor.in/API/V1/$smsApiKey/SMS/VERIFY/$verifySession/$otp');

    if (response.statusCode == 200) {
      Map<String, dynamic> verfiyDetails = jsonDecode(response.body);

      if (verfiyDetails['Status'] == "Success" &&
          verfiyDetails["Details"] == "OTP Matched") {
        FlushbarHelper.createSuccess(
                title: "Verified Successfully !!",
                message: "Thank you.",
                duration: Duration(seconds: 1))
            .show(context)
            .then((_) {
          print("VERIFIED ${widget.phoneNumber}");
          ExtendedNavigator.of(context).pushReplacementNamed(
            Routes.cwRegistration,
            arguments: CwRegistrationArguments(
              userGoogleInfo: widget.userGoogleInfo,
              phoneNumber: widget.phoneNumber,
            ),
          );
        });
      } else {
        FlushbarHelper.createError(
                title: "Please Enter the correct OTP",
                message: "Please try again.",
                duration: Duration(seconds: 1))
            .show(context);
      }
    } else {
      FlushbarHelper.createError(
              title: "Please Enter the correct OTP",
              message: "Please try again.",
              duration: Duration(seconds: 1))
          .show(context);
    }
  }

  @override
  void dispose() {
    errorController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      key: scaffoldKey,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(height: 90),
                buildTitle("Mobile Verification"),
                const SizedBox(height: 20),
                buildSubtitle("Enter the code sent to ${widget.phoneNumber}"),
                SizedBox(height: 40),
                Form(
                  key: formKey,
                  child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 30),
                      child: PinCodeTextField(
                        length: 6,
                        obsecureText: false,
                        animationType: AnimationType.fade,
                        validator: (v) {
                          if (v.isEmpty) {
                            return "Otp cannot be empty";
                          } else if (v.length != 6) {
                            return "Otp must be of 6 digits.";
                          } else {
                            return null;
                          }
                        },
                        pinTheme: PinTheme(
                          inactiveFillColor: Colors.grey,
                          inactiveColor: Colors.grey,
                          shape: PinCodeFieldShape.box,
                          borderRadius: BorderRadius.circular(1),
                          fieldHeight: 50,
                          fieldWidth: 40,
                          activeFillColor:
                              hasError ? Colors.orange : Colors.white,
                        ),
                        animationDuration: Duration(milliseconds: 300),
                        backgroundColor: Colors.black,
                        enableActiveFill: true,
                        errorAnimationController: errorController,
                        controller: textEditingController,
                        onCompleted: (v) {
                          print("#CW Completed $v");
                        },
                        onChanged: (value) {
                          print(value);
                          setState(() {
                            currentText = value;
                          });
                        },
                        beforeTextPaste: (text) {
                          return true;
                        },
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Text(
                    hasError ? "*Please fill up all the cells properly" : "",
                    style: TextStyle(
                        color: Colors.red,
                        fontSize: 12,
                        fontWeight: FontWeight.w400),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      text: "Didn't receive the code? ",
                      style: TextStyle(color: Colors.white, fontSize: 15),
                      children: [
                        TextSpan(
                            text: " RESEND",
                            recognizer: onTapRecognizer,
                            style: TextStyle(
                                color: Color(0xFF91D3B3),
                                fontWeight: FontWeight.bold,
                                fontSize: 16))
                      ]),
                ),
                SizedBox(
                  height: 34,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    onPressed: () async {
                      formKey.currentState.validate();
                      // conditions for validating
                      if (currentText.length != 6) {
                        errorController.add(ErrorAnimationType
                            .shake); // Triggering error shake animation
                        setState(() {
                          hasError = true;
                        });
                      } else {
                        verifyOTP(currentText);
                      }
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)),
                    elevation: 50,
                    padding: EdgeInsets.all(13),
                    color: Colors.blue[600],
                    child: Text(
                      "VERIFY",
                      style: TextStyle(color: Colors.white, fontSize: 15),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                SizedBox(
                  height: 26,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)),
                    elevation: 50,
                    padding: EdgeInsets.all(13),
                    color: Colors.grey,
                    child: Text("CLEAR"),
                    onPressed: () {
                      textEditingController.clear();
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
