import 'package:auto_route/auto_route.dart';
import 'package:cloudwalker_companion/domain/google_sign_in/models/user_google_info.dart';
import 'package:cloudwalker_companion/presentation/core/widgets/sub_title.dart';
import 'package:cloudwalker_companion/presentation/core/widgets/title.dart';
import 'package:cloudwalker_companion/presentation/routes/router.gr.dart';
import 'package:flushbar/flushbar_helper.dart';
import 'package:flutter/material.dart';

class MobileVerification extends StatefulWidget {
  final UserGoogleInfo userGoogleInfo;

  const MobileVerification({Key key, @required this.userGoogleInfo})
      : super(key: key);

  @override
  _MobileVerificationState createState() => _MobileVerificationState();
}

class _MobileVerificationState extends State<MobileVerification> {
  final _formKey = GlobalKey<FormState>();
  final _phoneNumberController = TextEditingController();
  bool isMobileVerified = false;

  @override
  void dispose() {
    super.dispose();
    _phoneNumberController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          padding: EdgeInsets.all(20),
          child: Column(
            children: [
              const SizedBox(height: 90),
              buildTitle("Mobile Verification"),
              const SizedBox(height: 20),
              buildSubtitle("We will send you a One time SMS message"),
              buildSubtitle("Carrier rates may apply"),
              const SizedBox(height: 50),
              TextFormField(
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.language),
                  labelText: 'Country Code',
                ),
                initialValue: "India (+91)",
                enabled: false,
              ),
              const SizedBox(height: 20),
              _buildPhoneNumberField(context),
              SizedBox(height: 60),
              _buildSaveButton(context)
            ],
          ),
        ),
      ),
    );
  }

  TextFormField _buildPhoneNumberField(BuildContext context) {
    return TextFormField(
      controller: _phoneNumberController,
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.phone),
        labelText: 'Phone Number',
      ),
      autocorrect: false,
      autofocus: false,
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        if (input.isEmpty) {
          return "Field cannot be empty";
        } else if (input.length != 10) {
          return "Phone Number must be of 10 digits.";
        }
        FocusScope.of(context).unfocus();
        return null;
      },
    );
  }

  Widget _buildSaveButton(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: RaisedButton(
        onPressed: () async {
          if (_formKey.currentState.validate()) {
            _formKey.currentState.save();
            ExtendedNavigator.of(context).pushPicodeScreen(
              phoneNumber: _phoneNumberController.text,
              userGoogleInfo: widget.userGoogleInfo,
            );
          } else {
            FlushbarHelper.createError(
                    title: "Form Invalid",
                    message: "Invalid Form. Please check the fields.",
                    duration: Duration(seconds: 1))
                .show(context);
          }
        },
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        elevation: 50,
        padding: EdgeInsets.all(13),
        color: Colors.blue[600],
        child: Text(
          "Verify",
          style: TextStyle(color: Colors.white, fontSize: 15),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
