import 'package:cloudwalker_companion/domain/search/models/cats.dart';
import 'package:cloudwalker_companion/presentation/search/card_scroll_widget.dart';
import 'package:flutter/material.dart';

var cardAspectRatio = 12.0 / 16.0;
var widgetAspectRatio = cardAspectRatio * 1.2;

class CatsViewer extends StatefulWidget {
  final Cats data;

  const CatsViewer({Key key, this.data}) : super(key: key);

  @override
  _CatsViewerState createState() => _CatsViewerState();
}

class _CatsViewerState extends State<CatsViewer> {
  var currentPage;

  @override
  void initState() {
    super.initState();
    currentPage = widget.data.rows[0].rowItems.length - 1.0;
  }

  @override
  Widget build(BuildContext context) {
    PageController controller =
        PageController(initialPage: widget.data.rows[0].rowItems.length - 1);
    controller.addListener(() {
      setState(() {
        currentPage = controller.page;
      });
    });

    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: Stack(
          children: <Widget>[
            CardScrollWidget(
              currentPage: currentPage,
              data: widget.data.rows[0].rowItems,
            ),
            Positioned.fill(
              child: PageView.builder(
                itemCount: widget.data.rows[0].rowItems.length,
                controller: controller,
                reverse: true,
                itemBuilder: (context, index) {
                  return Container();
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
