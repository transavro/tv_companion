import 'package:cloudwalker_companion/domain/search/models/cats.dart';
import 'package:cloudwalker_companion/presentation/search/cats_viewer.dart';
import 'package:cloudwalker_companion/presentation/search/cde_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchTab extends StatefulWidget {
  @override
  _SearchTabState createState() => _SearchTabState();
}

class _SearchTabState extends State<SearchTab> {
  Future<Cats> _makeDataCall() async {
    print("CATS CALLLING....");
    return Cats.fromJson(
        await DefaultAssetBundle.of(context).loadString('assets/cats.json'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text("Movie Box",
            style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
        actions: [
          CircleAvatar(
            backgroundColor: Colors.blue,
            radius: 18,
            child: IconButton(
              padding: EdgeInsets.zero,
              icon: Icon(Icons.search, size: 20.0),
              color: Colors.white,
              onPressed: () {
                showSearch(
                  context: context,
                  delegate: CDESearch(),
                );
              },
            ),
          ),
          SizedBox(width: 20)
        ],
      ),
      body: FutureBuilder<Cats>(
        future: _makeDataCall(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return CatsViewer(data: snapshot.data);
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}

// Future postWithClientCertificate() async {
//   var context = SecurityContext.defaultContext;
//   context.useCertificateChain('client-2048.crt');
//   context.usePrivateKey('client-2048.key', password: 'keyfile_password');
//   HttpClient client = new HttpClient(context: context);

//   // The rest of this code comes from your question.
//   var uri = "https://tvapi.cloudwalker.tv/cats.json";
//   var data = 'username=xxxxxxxx&password=xxxxxxxx';
//   var appKey = 'xxxxxxxxxxxx';
//   var method = 'POST';

//   var request = await client.openUrl(method, Uri.parse(uri));
//   request.headers
//       .set(HttpHeaders.CONTENT_TYPE, 'application/x-www-form-urlencoded');
//   request.headers.set('Accept-Version', '1.0.0');
//   request.write(data);
//   var response = await request.close();
//   // Process the response.
// }
