import 'package:cloudwalker_companion/domain/search/models/protos/CDEService.pbgrpc.dart';
import 'package:cloudwalker_companion/injection.dart';
import 'package:flutter/material.dart';

class CDESearch extends SearchDelegate<String> {
  @override
  String get searchFieldLabel => 'Content Discovery Engine';

  @override
  ThemeData appBarTheme(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return theme.copyWith(
        inputDecorationTheme: InputDecorationTheme(
          hintStyle: TextStyle(
            color: Colors.grey,
            fontSize: 12.0,
          ),
        ),
        primaryColor: Colors.black,
        scaffoldBackgroundColor: Colors.black,
        backgroundColor: Colors.black,
        textTheme: theme.textTheme.copyWith(
            title: theme.textTheme.title.copyWith(
                color: theme.primaryTextTheme.title.color, fontSize: 15.0)));
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      CircleAvatar(
        backgroundColor: Colors.blue,
        radius: 18,
        child: IconButton(
          padding: EdgeInsets.zero,
          icon: Icon(Icons.clear, size: 20.0),
          color: Colors.white,
          onPressed: () {
            query = "";
          },
        ),
      ),
      SizedBox(width: 20)
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return FutureBuilder<SearchResponse>(
      future: getIt<CDEServiceClient>().search(SearchQuery()..query = query),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<Content> contentSet = List();
          snapshot.data.contentTile.forEach(
            (e) {
              // if ((e.portriat.length > 0 ||
              //         e.poster.length > 0 ||
              //         e.banner.length > 0) &&
              //     (e.target.length > 0)) {
              //   contentSet.add(e);
              // }

              if ((e.portriat.length > 0 || e.poster.length > 0) &&
                  (e.play.length > 0)) {
                contentSet.add(e);
              }
            },
          );
          return GridView.builder(
            shrinkWrap: true,
            padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 8.0,
                mainAxisSpacing: 8.0,
                childAspectRatio: 0.66),
            itemCount: contentSet.length,
            itemBuilder: (context, index) {
              return _searchCard(context, getImageUrl(contentSet[index]),
                  contentSet[index].title);
            },
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _searchCard(BuildContext context, String imageUrl, String title) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(16.0),
      child: Container(
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
              color: Colors.black12, offset: Offset(3.0, 6.0), blurRadius: 10.0)
        ]),
        child: AspectRatio(
          aspectRatio: 12.0 / 16.0,
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Image.network(imageUrl, fit: BoxFit.cover),
              Align(
                alignment: Alignment.bottomLeft,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                      child: Text(
                        title,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 10.0,
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  String getImageUrl(Content content) {
    String imageUrl;
    if (content.portriat.length > 0) {
      if (content.portriat[0].startsWith("http") == false) {
        imageUrl = "http://asset.s4.cloudwalker.tv/images/tiles/" +
            content.portriat[0];
      } else {
        imageUrl = content.portriat[0];
      }
    } else if (content.poster.length > 0) {
      if (content.poster[0].startsWith("http") == false) {
        imageUrl =
            "http://asset.s4.cloudwalker.tv/images/tiles/" + content.poster[0];
      } else {
        imageUrl = content.poster[0];
      }
    }

    // else if (content.banner.length > 0) {
    //   if (content.banner[0].startsWith("http") == false) {
    //     imageUrl =
    //         "http://asset.s4.cloudwalker.tv/images/tiles/" + content.poster[0];
    //   } else {
    //     imageUrl = content.poster[0];
    //   }
    // }
    return imageUrl;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.isEmpty) {
      return Center(child: CircularProgressIndicator());
    } else {
      return FutureBuilder<SearchTextResponse>(
        future:
            getIt<CDEServiceClient>().searchText(SearchQuery()..query = query),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.separated(
              separatorBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Divider(
                    color: Colors.white,
                  ),
                );
              },
              itemBuilder: (context, index) => ListTile(
                onTap: () {
                  query = snapshot.data.result[index];
                  showResults(context);
                },
                dense: true,
                leading: Icon(Icons.local_movies),
                title: RichText(
                  text: TextSpan(
                    text:
                        snapshot.data.result[index].substring(0, query.length),
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                    children: [
                      TextSpan(
                        text:
                            snapshot.data.result[index].substring(query.length),
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              itemCount: snapshot.data.result.length,
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      );
    }
  }
}
