import 'package:flutter/material.dart';

typedef OnCwBtnPressed = void Function(String tag);

class BtnRow extends StatelessWidget {
  final Map<IconData, String> iconMap;
  final OnCwBtnPressed onCwBtnPressed;

  const BtnRow({Key key, this.iconMap, this.onCwBtnPressed}) : super(key: key);

  List<Widget> _buildIconBtns() {
    return iconMap.keys.map((e) {
      return IconButton(
        splashColor: Colors.blue[800],
        icon: Icon(e, color: Colors.white, size: 28),
        onPressed: () => onCwBtnPressed(iconMap[e]),
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: Color(0xFF222222),
        borderRadius: BorderRadius.all(Radius.circular(40.0)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: _buildIconBtns(),
      ),
    );
  }
}
