final Map<String, String> keycodeMap = Map();

loadKeyCodeMap() {
  //Dpad keycode
  keycodeMap["left"] = "105";
  keycodeMap["right"] = "106";
  keycodeMap["up"] = "103";
  keycodeMap["down"] = "108";
  keycodeMap["center"] = "28";

  //NumericKeycode
  keycodeMap["1"] = "2";
  keycodeMap["2"] = "3";
  keycodeMap["3"] = "4";
  keycodeMap["4"] = "5";
  keycodeMap["5"] = "6";
  keycodeMap["6"] = "7";
  keycodeMap["7"] = "8";
  keycodeMap["8"] = "9";
  keycodeMap["9"] = "10";
  keycodeMap["0"] = "11";

  // charKeycode qwertyuiop

  keycodeMap["q"] = "16";
  keycodeMap["w"] = "17";
  keycodeMap["e"] = "18";
  keycodeMap["r"] = "19";
  keycodeMap["t"] = "20";
  keycodeMap["y"] = "21";
  keycodeMap["u"] = "22";
  keycodeMap["i"] = "23";
  keycodeMap["o"] = "24";
  keycodeMap["p"] = "25";

  // charKeycode asdfghjkl
  keycodeMap["a"] = "30";
  keycodeMap["s"] = "31";
  keycodeMap["d"] = "32";
  keycodeMap["f"] = "33";
  keycodeMap["g"] = "34";
  keycodeMap["h"] = "35";
  keycodeMap["j"] = "36";
  keycodeMap["k"] = "37";
  keycodeMap["l"] = "38";

  //charKeycode zxcvbnm
  keycodeMap["z"] = "44";
  keycodeMap["x"] = "45";
  keycodeMap["c"] = "46";
  keycodeMap["v"] = "47";
  keycodeMap["b"] = "48";
  keycodeMap["n"] = "49";
  keycodeMap["m"] = "50";

  //special
  keycodeMap["capslock"] = "58";
  keycodeMap["space"] = "57";
  keycodeMap["backspace"] = "14";
  keycodeMap["rightshift"] = "54";
  keycodeMap["enter"] = "28";
  keycodeMap["home"] = "102";
  keycodeMap["volumeup"] = "115";
  keycodeMap["volumedown"] = "114";
  keycodeMap["mute"] = "113";
  keycodeMap["back"] = "158";
  keycodeMap["power"] = "116";

  keycodeMap["HDMI"] = "774";
  keycodeMap["HDMI1"] = "778";
  keycodeMap["HDMI2"] = "779";
  keycodeMap["ATV"] = "768";
  keycodeMap["AV"] = "772";
  keycodeMap["AV1"] = "781";
  keycodeMap["AV2"] = "782";
  keycodeMap["YPBPR"] = "773";
  keycodeMap["YPBPR1"] = "783";

  keycodeMap["settings"] = "249"; //249, 470

  keycodeMap["sources"] = "79";

  keycodeMap["play"] = "164"; //207
  keycodeMap["rewind"] = "168";
  keycodeMap["forward"] = "208";
  keycodeMap["stop"] = "128";
}

// key 168     MEDIA_REWIND
// key 164     MEDIA_PLAY_PAUSE
// key 208     MEDIA_FAST_FORWARD
// key 412     MEDIA_PREVIOUS
// key 407     MEDIA_NEXT
// key 207     MEDIA_PLAY
// key 119     MEDIA_PAUSE
// key 128     MEDIA_STOP
