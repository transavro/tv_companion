import 'package:flutter/material.dart';

typedef OnPressed = void Function();

class CircleIconBtn extends StatelessWidget {
  final OnPressed onPressed;
  final IconData icon;

  const CircleIconBtn({Key key, this.onPressed, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox.fromSize(
      size: Size(60, 60),
      child: ClipOval(
        child: Material(
          color: Color(0xFF222222),
          child: InkWell(
            splashColor: Colors.blue,
            onTap: onPressed,
            child: Icon(
              icon,
              color: Colors.white,
              size: 30,
            ),
          ),
        ),
      ),
    );
  }
}
