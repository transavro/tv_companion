import 'package:auto_route/auto_route.dart';
import 'package:cloudwalker_companion/application/tv_interaction/tv_interaction_bloc.dart';
import 'package:cloudwalker_companion/presentation/tv_remote/cwKeycode.dart';
import 'package:cloudwalker_companion/presentation/tv_remote/btn_row.dart';
import 'package:cloudwalker_companion/presentation/tv_remote/circle_btn.dart';
import 'package:cloudwalker_companion/presentation/tv_remote/joystick_view.dart';
import 'package:flushbar/flushbar_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:cloudwalker_companion/presentation/routes/router.gr.dart';

class TvRemoteTab extends StatefulWidget {
  bool timerSwitch = false;
  String deviceName = "Searching...";

  @override
  _TvRemoteTabState createState() => _TvRemoteTabState();
}

class _TvRemoteTabState extends State<TvRemoteTab> {
  String oldValue = "";
  final ScrollController _scrollController = ScrollController();
  List<String> tvSources;

  void _scrollToEnd() {
    if (!_scrollController.hasClients) {
      return;
    }

    var scrollPosition = _scrollController.position;

    if (scrollPosition.maxScrollExtent > scrollPosition.minScrollExtent) {
      _scrollController.animateTo(
        scrollPosition.maxScrollExtent,
        duration: new Duration(milliseconds: 200),
        curve: Curves.easeOut,
      );
    }
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TvInteractionBloc, TvInteractionState>(
      condition: (previousState, currentState) {
        if (currentState is TvInteractionLoading) {
          FlushbarHelper.createInformation(
                  message: "Search Cloud TV", duration: Duration(seconds: 2))
              .show(context);
          return false;
        } else if (currentState is NoWifiState) {
          FlushbarHelper.createError(
                  message: "Not connected to wifi.",
                  duration: Duration(seconds: 2))
              .show(context);
          return false;
        } else if (currentState is NoDevicesAvaliableState) {
          FlushbarHelper.createError(
                  message: "No Device avaliable.",
                  duration: Duration(seconds: 2))
              .show(context);
          return false;
        } else if (currentState is TvInteractionErrorState) {
          FlushbarHelper.createError(
                  title: "Tv Sync error",
                  duration: Duration(seconds: 2),
                  message: null)
              .show(context);
          return false;
        } else if (currentState is TvInteractionInitial) {
          return true;
        } else if (currentState is TvSuccesufullyConnectedState) {
          FlushbarHelper.createSuccess(
                  message: "Cloud TV Connected.",
                  duration: Duration(seconds: 2))
              .show(context);

          widget.deviceName = currentState.linkedTvName;
          setState(() {});
          return true;
        } else if (currentState is AvaliableDevicesState) {
          getSelectedDevice(context, currentState.devices);
          return false;
        } else if (currentState is TvSourcesState) {
          tvSources = currentState.tvSources;
          return false;
        } else {
          return false;
        }
      },
      builder: (context, state) {
        return _buildRemote(context);
      },
    );
  }

  String getIpAddress(String data) {
    RegExp re = new RegExp(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}');
    // First match
    Match firstMatch = re.firstMatch(data);
    return data.substring(firstMatch.start, firstMatch.end);
  }

  void getSelectedDevice(BuildContext context, Map networkDevices) async {
    if (networkDevices.length == 1) {
      networkDevices.forEach((key, value) {
        final ip = getIpAddress(key);
        final port = value;
        context.bloc<TvInteractionBloc>()..add(LinkAndConnectDevice(ip, port));
        context.bloc<TvInteractionBloc>()..add(TvSourcesEvent());
      });
    } else {
      FlushbarHelper.createSuccess(
              message: "Multiple Cloud Tv found.",
              duration: Duration(seconds: 1))
          .show(context)
          .then((value) async {
        Map<String, int> selectedDevice = await ExtendedNavigator.of(context)
            .pushNetworkDevices(avaliableDevices: networkDevices);

        if (selectedDevice.length == 1) {
          selectedDevice.forEach((key, value) {
            final ip = getIpAddress(key);
            final port = value;
            context.bloc<TvInteractionBloc>()
              ..add(LinkAndConnectDevice(ip, port));
            context.bloc<TvInteractionBloc>()..add(TvSourcesEvent());
          });
        }
      });
    }
  }

  Widget _searchingRow() {
    if (widget.deviceName == "Searching...") {
      return Row(
        children: [
          Text(
            widget.deviceName,
            style: TextStyle(
              fontSize: 12,
              fontStyle: FontStyle.normal,
              color: widget.deviceName == "No Connection."
                  ? Colors.red
                  : Colors.white,
            ),
          ),
          SizedBox(width: 90),
          Container(
            width: 15,
            height: 15,
            child: CircularProgressIndicator(
              strokeWidth: 3,
            ),
          ),
        ],
      );
    } else {
      return Text(
        widget.deviceName,
        style: TextStyle(
          fontSize: 12,
          fontStyle: FontStyle.normal,
          color:
              widget.deviceName == "No Connection." ? Colors.red : Colors.white,
        ),
      );
    }
  }

  Widget _buildRemote(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(45.0),
        child: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.black,
          title: _searchingRow(),
          centerTitle: true,
          actions: [
            CircleAvatar(
              backgroundColor: Colors.blue,
              radius: 18,
              child: IconButton(
                padding: EdgeInsets.zero,
                icon: Icon(Icons.refresh, size: 20.0),
                color: Colors.white,
                onPressed: () {
                  context.bloc<TvInteractionBloc>()..add(GetAvaliableDevices());
                },
              ),
            ),
            SizedBox(width: 20),
          ],
          leading: IconButton(
            icon: Image.asset('assets/images/cwtv.png',
                width: 30, height: 30, color: Colors.white),
            onPressed: null,
          ),
          titleSpacing: 0.0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Divider(color: Colors.grey, height: 2.0),
            JoystickView(
              backgroundColor: Color(0xFF222222),
              iconsColor: Colors.white,
              innerCircleColor: Colors.blue,
              size: 220,
              onCwNavPressed: (tag) {
                print("##NAV PRESSED  $tag");
                if ("up" == tag) {
                  context.bloc<TvInteractionBloc>()..add(TvKeyCodeEvent("103"));
                } else if ("down" == tag) {
                  context.bloc<TvInteractionBloc>()..add(TvKeyCodeEvent("108"));
                } else if ("left" == tag) {
                  context.bloc<TvInteractionBloc>()..add(TvKeyCodeEvent("105"));
                } else if ("right" == tag) {
                  context.bloc<TvInteractionBloc>()..add(TvKeyCodeEvent("106"));
                } else if ("center" == tag) {
                  context.bloc<TvInteractionBloc>()..add(TvKeyCodeEvent("28"));
                }
              },
            ),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CircleIconBtn(
                  icon: Icons.home,
                  onPressed: () {
                    return context.bloc<TvInteractionBloc>()
                      ..add(TvKeyCodeEvent(keycodeMap["home"]));
                  },
                ),
                CircleIconBtn(
                  icon: Icons.apps,
                  onPressed: () {
                    return context.bloc<TvInteractionBloc>()
                      ..add(GetAllAppsEvent());
                  },
                ),
                CircleIconBtn(
                  icon: Icons.arrow_back,
                  onPressed: () {
                    return context.bloc<TvInteractionBloc>()
                      ..add(TvKeyCodeEvent(keycodeMap["back"]));
                  },
                ),
              ],
            ),
            SizedBox(height: 20),
            BtnRow(
              onCwBtnPressed: (tag) => context.bloc<TvInteractionBloc>()
                ..add(TvKeyCodeEvent(keycodeMap[tag])),
              iconMap: <IconData, String>{
                Icons.remove: "volumedown",
                Icons.volume_off: "mute",
                Icons.add: "volumeup"
              },
            ),
            SizedBox(height: 20),
            BtnRow(
              onCwBtnPressed: (tag) {
                if (tag == "keyboard") {
                  _scrollToEnd();
                } else {
                  return context.bloc<TvInteractionBloc>()
                    ..add(TvKeyCodeEvent(keycodeMap[tag]));
                }
              },
              iconMap: <IconData, String>{
                Icons.videogame_asset: "sources",
                Icons.settings: "settings",
                Icons.settings_power: "power"
              },
            ),
            SizedBox(height: 20),
            BtnRow(
              onCwBtnPressed: (tag) => context.bloc<TvInteractionBloc>()
                ..add(TvKeyCodeEvent(keycodeMap[tag])),
              iconMap: <IconData, String>{
                Icons.play_arrow: "play",
                Icons.fast_rewind: "rewind",
                Icons.fast_forward: "forward",
                Icons.stop: "stop"
              },
            ),
            // SizedBox(height: 20),
            // TvKeyboard(
            //   cwKeyPressed: (tag) => context.bloc<TvInteractionBloc>()
            //     ..add(TvKeyCodeEvent(keycodeMap[tag])),
            // ),
          ],
        ),
      ),
    );
  }

  void _showTvSourceDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          backgroundColor: Color(0xFF222222),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
          elevation: 16,
          child: Container(
              height: 200.0,
              width: 100.0,
              child: ListView.separated(
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(tvSources[index],
                          style: TextStyle(fontSize: 10)),
                      onTap: () {
                        context.bloc<TvInteractionBloc>()
                          ..add(TvKeyCodeEvent(keycodeMap[tvSources[index]]));
                        Navigator.of(context).pop(false);
                      },
                      hoverColor: Colors.blue,
                      focusColor: Colors.blue,
                      leading: Icon(Icons.settings_input_hdmi),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Divider(
                        color: Colors.white,
                      ),
                    );
                  },
                  itemCount: tvSources.length)),
        );
      },
    );
  }
}
