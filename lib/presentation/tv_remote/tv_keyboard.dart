import 'package:flutter/material.dart';
import 'package:virtual_keyboard/virtual_keyboard.dart';

typedef OnCwKeyBoardBtnPressed = void Function(String tag);

class TvKeyboard extends StatelessWidget {
  final OnCwKeyBoardBtnPressed cwKeyPressed;
  String text = "";
  bool shiftEnabled = false;

  TvKeyboard({Key key, this.cwKeyPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFF222222),
      child: VirtualKeyboard(
        height: 240,
        textColor: Colors.white,
        type: VirtualKeyboardType.Alphanumeric,
        onKeyPress: _onKeyPress,
      ),
    );
  }

  _onKeyPress(VirtualKeyboardKey key) {
    if (key.keyType == VirtualKeyboardKeyType.String) {
      print("KEYTEXT ${key.text}");
      cwKeyPressed(key.text);
    } else if (key.keyType == VirtualKeyboardKeyType.Action) {
      switch (key.action) {
        case VirtualKeyboardKeyAction.Backspace:
          cwKeyPressed("backspace");
          break;
        case VirtualKeyboardKeyAction.Return:
          cwKeyPressed("rightshift");
          cwKeyPressed("enter");
          break;
        case VirtualKeyboardKeyAction.Space:
          cwKeyPressed("space");
          break;
        case VirtualKeyboardKeyAction.Shift:
          shiftEnabled = !shiftEnabled;
          cwKeyPressed("capslock");
          break;
        default:
      }
    }
  }
}
