import 'package:auto_route/auto_route.dart';
import 'package:cloudwalker_companion/application/auth/auth_bloc.dart';
import 'package:cloudwalker_companion/domain/cw_registration/models/protos/User.pb.dart';
import 'package:cloudwalker_companion/domain/cw_registration/models/protos/google/type/date.pb.dart';
import 'package:cloudwalker_companion/infrastructure/core/app_utils.dart';
import 'package:cloudwalker_companion/presentation/core/widgets/title.dart';
import 'package:cloudwalker_companion/presentation/cw_registration/widgets/always_disabled_focus_node.dart';
import 'package:cloudwalker_companion/presentation/cw_registration/widgets/cw_preferences.dart';
import 'package:cloudwalker_companion/presentation/routes/router.gr.dart';
import 'package:flushbar/flushbar_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserProfileTab extends StatefulWidget {
  User user;
  UserProfileTab({Key key, this.user}) : super(key: key);

  @override
  _UserProfileTabState createState() => _UserProfileTabState();
}

class _UserProfileTabState extends State<UserProfileTab> {
  final _formKey = GlobalKey<FormState>();
  User user;
  DateTime _dateTime;
  bool inUpdateMode = false;
  List gender = ["Male", "Female", "Other"];
  String select;

  Map<String, bool> languagesCwMap = {
    "Arabic": false,
    "Assamese": false,
    "Bengali": false,
    "Czech": false,
    "Dutch": false,
    "English": false,
    "Flemish": false,
    "French": false,
    "German": false,
    "Gujarati": false,
    "Hebrew": false,
    "Hindi": false,
    "Japanese": false,
    "Kannada": false,
    "Khmer": false,
    "Korean": false,
    "Malayalam": false,
    "Mandarin": false,
    "Marathi": false,
    "Norwegian": false,
    "Persian": false,
    "Portuguese": false,
    "Punjabi": false,
    "Russian": false,
    "Sanskrit": false,
    "Spanish": false,
    "Tamil": false,
    "Telugu": false,
    "Turkish": false,
    "Urdu": false,
  };

  Map<String, bool> contentTypeCwMap = {
    "Apps": false,
    "Awards": false,
    "Cookery": false,
    "Devotional Videos": false,
    "Documentary": false,
    "Fitness": false,
    "Funny Videos": false,
    "Kids": false,
    "Motivational Videos": false,
    "Movie Trailers": false,
    "Movies": false,
    "Music ": false,
    "Popular Videos": false,
    "Series": false,
    "Short Film": false,
    "TalkShow": false,
    "Wild Life": false,
  };

  Map<String, bool> genreCwMap = {
    "Action": false,
    "Adventure": false,
    "Animation": false,
    "Art": false,
    "Arts And Artist": false,
    "Automobiles": false,
    "Biography": false,
    "Comedy": false,
    "Conspiracy": false,
    "Cookeryshow": false,
    "Crime": false,
    "Culture": false,
    "Dance": false,
    "Dark Comedy": false,
    "Devotional": false,
    "Disaster": false,
    "Documentary": false,
    "Drama": false,
    "Drugs": false,
    "Economics": false,
    "Education": false,
    "Entertainment": false,
    "Environment": false,
    "Family": false,
    "Fantasy": false,
    "Finance": false,
    "Game Show": false,
    "History": false,
    "Horror": false,
    "Infotainment": false,
    "Investigation": false,
    "Life": false,
    "Medical": false,
    "Military": false,
    "Music": false,
    "Musical": false,
    "Mystery": false,
    "Mythology": false,
    "Nature": false,
    "News": false,
    "People": false,
    "Police": false,
    "Politics": false,
    "Reality": false,
    "Religion": false,
    "Romance": false,
    "Science": false,
    "Sci-Fi": false,
    "Short": false,
    "Social": false,
    "Society": false,
    "Space": false,
    "Sports": false,
    "Standup Comedy": false,
    "Suspence": false,
    "Talk Show": false,
    "Technology": false,
    "Thriller": false,
    "Travel": false,
    "Visual": false,
    "War": false,
    "Western": false,
    "Wildlife": false,
  };

  TextEditingController genreController;
  TextEditingController languageController;
  TextEditingController typeController;
  TextEditingController dateOfBirthController;

  @override
  void initState() {
    user = widget.user;
    //setting maps
    settingMaps();
    //setting initial values
    genreController = TextEditingController(
        text: user.genre.toString().replaceAll("[", "").replaceAll("]", ""));
    languageController = TextEditingController(
        text: user.language.toString().replaceAll("[", "").replaceAll("]", ""));
    typeController = TextEditingController(
        text: user.contentType
            .toString()
            .replaceAll("[", "")
            .replaceAll("]", ""));
    dateOfBirthController = TextEditingController(
        text:
            "${user.dateOfBirth.day.toString()}-${user.dateOfBirth.month.toString()}-${user.dateOfBirth.year.toString()}");
    super.initState();
  }

  settingMaps() async {
    user.genre.forEach((element) {
      element = element.replaceAll(" ", "");
      genreCwMap.update(element, (value) => true);
    });

    user.language.forEach((element) {
      element = element.replaceAll(" ", "");
      languagesCwMap.update(element, (value) => true);
    });

    user.contentType.forEach((element) {
      element = element.replaceAll(" ", "");
      contentTypeCwMap.update(element, (value) => true);
    });
  }

  @override
  void dispose() {
    genreController.dispose();
    languageController.dispose();
    typeController.dispose();
    dateOfBirthController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: buildTitle("Your Profile"),
        centerTitle: true,
        backgroundColor: Colors.black,
        actions: [
          SizedBox(width: 20),
          IconButton(
            icon: Icon(
              Icons.mode_edit,
              color: Colors.white,
              size: 28,
            ),
            splashColor: Colors.blue[900],
            splashRadius: 20.0,
            onPressed: () {
              inUpdateMode = true;
              setState(() {});
            },
          ),
          SizedBox(width: 20),
        ],
      ),
      body: BlocConsumer<AuthBloc, AuthState>(
        listener: (context, state) async {
          if (state is NoNetworkState) {
            FlushbarHelper.createError(
                    message: "No Internet", duration: Duration(seconds: 1))
                .show(context);
          } else if (state is AuthServerErrorState) {
            FlushbarHelper.createError(
                    message: state.errorMessage, duration: Duration(seconds: 1))
                .show(context);
          } else if (state is AuthLoadingState) {
            FlushbarHelper.createLoading(
                    message: "Processing...",
                    duration: Duration(seconds: 1),
                    linearProgressIndicator: LinearProgressIndicator())
                .show(context);
          } else if (state is NoAuthState) {
            FlushbarHelper.createSuccess(
              message: "User deleted Successfully",
              duration: Duration(seconds: 1),
            ).show(context).whenComplete(() {
              ExtendedNavigator.of(context)
                  .pushReplacementNamed(Routes.cwGoogleSignIn);
            });
          } else if (state is UpdateSuccessfulState) {
            FlushbarHelper.createSuccess(
              message: "User updated Successfully",
              duration: Duration(seconds: 1),
            ).show(context);
          } else if (state is RemovedTvDeviceSuccessfullyState) {
            FlushbarHelper.createSuccess(
              message: "Removed TV Device Successfully",
              duration: Duration(seconds: 1),
            ).show(context);
          }
        },
        builder: (context, state) {
          return Form(
            key: _formKey,
            child: SingleChildScrollView(
              padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: _formFileds(),
              ),
            ),
          );
        },
      ),
    );
  }

  List<Widget> _formFileds() {
    List<Widget> fields = List();
    // fields.add(const SizedBox(height: 60));
    fields.add(_buildProfileIcon(context));
    fields.add(const SizedBox(height: 23));
    fields.add(Text("User Info",
        style: TextStyle(color: Colors.blue[600], fontSize: 12)));
    fields.add(const SizedBox(height: 13));
    fields.add(_buildFirstNameField(context));
    fields.add(const SizedBox(height: 13));
    fields.add(_buildLastNameField(context));
    fields.add(const SizedBox(height: 13));
    fields.add(_buildEmailField());
    fields.add(const SizedBox(height: 13));
    fields.add(_buildPhoneNumberField(context));
    fields.add(const SizedBox(height: 13));
    fields.add(_buildDateOfBirthField(context));
    fields.add(const SizedBox(height: 13));
    fields.add(
        inUpdateMode ? _buildGenderRadio(context) : _buildGenderField(context));
    fields.add(const SizedBox(height: 13));
    fields.add(const SizedBox(height: 13));
    fields.add(Text("User Preferences",
        style: TextStyle(color: Colors.blue[600], fontSize: 12)));

    fields.add(const SizedBox(height: 13));
    fields.add(_buildGenreField(context));
    fields.add(const SizedBox(height: 13));
    fields.add(_buildLanguageField(context));
    fields.add(const SizedBox(height: 13));
    fields.add(_buildContentTypeField(context));
    fields.add(const SizedBox(height: 13));
    fields.add(const SizedBox(height: 13));
    fields.add(Text("Linked Devices",
        style: TextStyle(color: Colors.blue[600], fontSize: 12)));
    fields.add(const SizedBox(height: 13));
    fields.add(_buildLinkedDeviceList(context));
    if (inUpdateMode) {
      fields.add(const SizedBox(height: 30));
      fields.add(_buildDeleteButton(context));
      fields.add(const SizedBox(height: 30));
      fields.add(_buildSaveButton(context));
      fields.add(const SizedBox(height: 30));
    }
    return fields;
  }

  Widget _buildLinkedDeviceList(BuildContext context) {
    return FutureBuilder(
      future: getLinkedDevicesFromPrefs(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          user.linkedDevices.clear();
          user.linkedDevices.addAll(snapshot.data);
          return ListView.separated(
            padding: EdgeInsets.zero,
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                leading: Icon(Icons.tv),
                title: Text(
                  user.linkedDevices.elementAt(index).tvName,
                  style: TextStyle(fontSize: 12),
                ),
                trailing: IconButton(
                  icon: Icon(Icons.delete),
                  splashColor: Colors.blue[600],
                  onPressed: () {
                    context.bloc<AuthBloc>()
                      ..add(RemoveTvDevice(
                          user.linkedDevices.elementAt(index).tvEmac));
                  },
                ),
                dense: true,
                isThreeLine: false,
                contentPadding: EdgeInsets.zero,
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return Divider(color: Colors.white);
            },
            itemCount: user.linkedDevices.length,
          );
        } else {
          return Text("No Device Linked Yet.");
        }
      },
    );
  }

  TextFormField _buildDateOfBirthField(BuildContext context) {
    return TextFormField(
        controller: dateOfBirthController,
        keyboardType: TextInputType.datetime,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.cake),
          labelText: 'Date of Birth',
        ),
        autocorrect: false,
        enabled: inUpdateMode,
        style: TextStyle(fontSize: 12),
        enableInteractiveSelection: false,
        focusNode: AlwaysDisabledFocusNode(),
        onTap: () {
          showDatePicker(
                  context: context,
                  initialDate: _dateTime == null ? DateTime.now() : _dateTime,
                  firstDate: DateTime(1965),
                  lastDate: DateTime(2030))
              .then((date) {
            setState(() {
              _dateTime = date;
              dateOfBirthController.text =
                  "${date.day}-${date.month}-${date.year}";
            });
          });
        },
        validator: (String input) {
          if (input.isEmpty) {
            return "Field cannot be empty";
          } else if (input.split("-").length != 3) {
            return "Invalid Date";
          } else if (int.parse(input.split("-")[0]) > 2005) {
            return "Age should be greater than 15";
          }
          return null;
        },
        onSaved: (value) {
          List<String> tmp = value.split('-');
          Date pbDate = Date();
          pbDate.day = int.parse(tmp[0]);
          pbDate.month = int.parse(tmp[1]);
          pbDate.year = int.parse(tmp[2]);
          user.dateOfBirth = pbDate;
        });
  }

  Row addRadioButton(int btnValue, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          activeColor: Theme.of(context).primaryColor,
          value: gender[btnValue],
          groupValue: select,
          onChanged: (value) {
            setState(() {
              print(value);
              select = value;
              if (value == "Male") {
                user.gender = Gender.MALE;
              } else if (value == "Female") {
                user.gender = Gender.FEMALE;
              } else {
                user.gender = Gender.NOTDEFINED;
              }
            });
          },
        ),
        Text(title, style: TextStyle(fontSize: 12))
      ],
    );
  }

  Widget _buildGenderRadio(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Gender", style: TextStyle(fontSize: 12, color: Colors.blue[600])),
        Row(
          children: <Widget>[
            addRadioButton(0, 'Male'),
            addRadioButton(1, 'Female'),
            addRadioButton(2, 'Others'),
          ],
        )
      ],
    );
  }

  TextFormField _buildGenreField(BuildContext context) {
    return TextFormField(
      controller: genreController,
      enableInteractiveSelection: false,
      focusNode: AlwaysDisabledFocusNode(),
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.movie_filter),
        labelText: 'Genres',
      ),
      onTap: () async {
        Map result = await Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) {
          return CwPrefrence(
            tag: "Genres",
            targetMap: genreCwMap,
          );
        }));

        if (result != null && result.length > 0) {
          user.genre
            ..clear()
            ..addAll(result.keys);
          setState(() {
            genreController.text = result.keys.join(", ").toString();
          });
        }
      },
      autocorrect: false,
      enabled: inUpdateMode,
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        if (input.isEmpty) {
          return "Field cannot be empty";
        } else if (input.split(',').length < 4) {
          return "Select atleast 4 Genres";
        }
        return null;
      },
      onSaved: (value) => {},
    );
  }

  TextFormField _buildContentTypeField(BuildContext context) {
    return TextFormField(
      controller: typeController,
      enableInteractiveSelection: false,
      focusNode: AlwaysDisabledFocusNode(),
      onTap: () async {
        Map result = await Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) {
          return CwPrefrence(
            tag: "Content Types",
            targetMap: contentTypeCwMap,
          );
        }));

        if (result != null && result.length > 0) {
          user.contentType
            ..clear()
            ..addAll(result.keys);
          setState(() {
            typeController.text = result.keys.join(", ").toString();
          });
        }
      },
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.movie_creation),
        labelText: 'Content Type',
      ),
      autocorrect: false,
      enabled: inUpdateMode,
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        if (input.isEmpty) {
          return "Field cannot be empty";
        } else if (input.split(',').length < 4) {
          return "Select atleast 4 Content Types";
        }
        return null;
      },
      onSaved: (value) => {},
    );
  }

  TextFormField _buildLanguageField(BuildContext context) {
    return TextFormField(
      controller: languageController,
      enableInteractiveSelection: false,
      focusNode: AlwaysDisabledFocusNode(),
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.language),
        labelText: 'Languages',
      ),
      autocorrect: false,
      enabled: inUpdateMode,
      onTap: () async {
        Map result = await Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) {
          return CwPrefrence(
            tag: "Languages",
            targetMap: languagesCwMap,
          );
        }));

        if (result != null && result.length > 0) {
          user.language
            ..clear()
            ..addAll(result.keys);
          setState(() {
            languageController.text = result.keys.join(", ").toString();
          });
        }
      },
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        if (input.isEmpty) {
          return "Field cannot be empty";
        } else if (input.split(',').length < 4) {
          return "Select atleast 4 Languages";
        }
        return null;
      },
      onSaved: (value) => {},
    );
  }

  TextFormField _buildEmailField() {
    return TextFormField(
      initialValue: user.email,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.email),
        labelText: 'Email',
      ),
      enabled: false,
      autocorrect: false,
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        if (input.isEmpty) {
          return "Field cannot be empty";
        }
        return null;
      },
      onSaved: (value) => user.email,
    );
  }

  TextFormField _buildFirstNameField(BuildContext context) {
    return TextFormField(
      initialValue: user.name.split(" ")[0],
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.person_pin),
        labelText: 'First Name',
      ),
      enabled: inUpdateMode,
      autocorrect: false,
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        Pattern pattern = r'^[A-Za-z]+(?:[ _-][A-Za-z]+)*$';
        RegExp regex = new RegExp(pattern);
        if (input.isEmpty) {
          return "Field cannot be empty";
        } else if (input.length < 3) {
          return "First name should be 3 character log";
        } else if (!regex.hasMatch(input))
          return 'Invalid First name';
        else
          return null;
      },
      onSaved: (value) => user.name = value,
    );
  }

  TextFormField _buildLastNameField(BuildContext context) {
    return TextFormField(
      initialValue: user.name.split(" ")[1],
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.person_pin),
        labelText: 'Last Name',
      ),
      autocorrect: false,
      enabled: inUpdateMode,
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        Pattern pattern = r'^[A-Za-z]+(?:[ _-][A-Za-z]+)*$';
        RegExp regex = new RegExp(pattern);
        if (input.isEmpty) {
          return "Field cannot be empty";
        } else if (input.length < 3) {
          return "First name should be 3 character log";
        } else if (!regex.hasMatch(input))
          return 'Invalid Last name';
        else
          return null;
      },
      onSaved: (value) {
        user.name = user.name + " " + value;
      },
    );
  }

  TextFormField _buildPhoneNumberField(BuildContext context) {
    return TextFormField(
      initialValue: user.phoneNumber,
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.phone),
        labelText: 'Phone Number',
      ),
      autocorrect: false,
      enabled: false,
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        if (input.isEmpty) {
          return "Field cannot be empty";
        } else if (input.length != 10) {
          return "Phone Number must be of 10 digits.";
        }
        return null;
      },
      onSaved: (value) => user.phoneNumber = value,
    );
  }

  TextFormField _buildGenderField(BuildContext context) {
    return TextFormField(
      initialValue: user.gender.name,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.favorite),
          labelText: 'Gender',
          hintText: 'Male/Female'),
      autocorrect: false,
      enabled: inUpdateMode,
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        if (input.isEmpty) {
          return "Field cannot be empty";
        } else if (input.toLowerCase() == "male" ||
            input.toLowerCase() == "female") {
          return null;
        } else {
          return "Gender must be MALE/FEMALE.";
        }
      },
      onSaved: (value) =>
          user.gender = value == 'male' ? Gender.MALE : Gender.FEMALE,
    );
  }

  Widget _buildProfileIcon(BuildContext context) {
    return Center(
        child: CircleAvatar(
      radius: 40.0,
      backgroundImage: NetworkImage(user.imageUrl),
      backgroundColor: Colors.transparent,
    ));
  }

  Widget _buildDeleteButton(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: RaisedButton(
        onPressed: () {
          context.bloc<AuthBloc>().add(DeleteRegisteredUser(user.googleId));
        },
        elevation: 50,
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0)),
        padding: EdgeInsets.all(13),
        color: Colors.grey[600],
        child: Text(
          "Delete",
          style: TextStyle(
            color: Colors.white,
            fontSize: 15,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Widget _buildSaveButton(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: RaisedButton(
        onPressed: () {
          if (_formKey.currentState.validate()) {
            //saving all the values
            _formKey.currentState.save();
            context.bloc<AuthBloc>().add(UpdateCurrentUser(user));
            inUpdateMode = false;
          } else {
            FlushbarHelper.createError(
                    title: "Form Invalid",
                    message: "Invalid Form. Please check the fields.",
                    duration: Duration(seconds: 1))
                .show(context);
          }
        },
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0)),
        elevation: 50,
        padding: EdgeInsets.all(13),
        color: Colors.blue[600],
        child: Text(
          "Save",
          style: TextStyle(
            color: Colors.white,
            fontSize: 15,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
