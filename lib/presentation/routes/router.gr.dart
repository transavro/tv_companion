// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/auto_route.dart';
import 'package:cloudwalker_companion/presentation/splash/splash_page.dart';
import 'package:cloudwalker_companion/presentation/google_sign_in/cw_google_sign_in.dart';
import 'package:cloudwalker_companion/presentation/cw_registration/cw_registration.dart';
import 'package:cloudwalker_companion/domain/google_sign_in/models/user_google_info.dart';
import 'package:cloudwalker_companion/presentation/home/home_page.dart';
import 'package:cloudwalker_companion/domain/cw_registration/models/protos/User.pb.dart';
import 'package:cloudwalker_companion/presentation/network_devices/network_devices.dart';
import 'package:cloudwalker_companion/presentation/mobile_verification/pin_code_verfication_screen.dart';
import 'package:cloudwalker_companion/presentation/mobile_verification/mobile_verification.dart';

abstract class Routes {
  static const splashPage = '/';
  static const cwGoogleSignIn = '/cw-google-sign-in';
  static const cwRegistration = '/cw-registration';
  static const homePage = '/home-page';
  static const networkDevices = '/network-devices';
  static const picodeScreen = '/picode-screen';
  static const mobileVerification = '/mobile-verification';
  static const all = {
    splashPage,
    cwGoogleSignIn,
    cwRegistration,
    homePage,
    networkDevices,
    picodeScreen,
    mobileVerification,
  };
}

class Router extends RouterBase {
  @override
  Set<String> get allRoutes => Routes.all;

  @Deprecated('call ExtendedNavigator.ofRouter<Router>() directly')
  static ExtendedNavigatorState get navigator =>
      ExtendedNavigator.ofRouter<Router>();

  @override
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.splashPage:
        return MaterialPageRoute<dynamic>(
          builder: (context) => SplashPage(),
          settings: settings,
        );
      case Routes.cwGoogleSignIn:
        return MaterialPageRoute<dynamic>(
          builder: (context) => CwGoogleSignIn(),
          settings: settings,
        );
      case Routes.cwRegistration:
        if (hasInvalidArgs<CwRegistrationArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<CwRegistrationArguments>(args);
        }
        final typedArgs = args as CwRegistrationArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) => CwRegistration(
              key: typedArgs.key,
              userGoogleInfo: typedArgs.userGoogleInfo,
              phoneNumber: typedArgs.phoneNumber),
          settings: settings,
        );
      case Routes.homePage:
        if (hasInvalidArgs<HomePageArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<HomePageArguments>(args);
        }
        final typedArgs = args as HomePageArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) =>
              HomePage(key: typedArgs.key, user: typedArgs.user),
          settings: settings,
        );
      case Routes.networkDevices:
        if (hasInvalidArgs<NetworkDevicesArguments>(args)) {
          return misTypedArgsRoute<NetworkDevicesArguments>(args);
        }
        final typedArgs =
            args as NetworkDevicesArguments ?? NetworkDevicesArguments();
        return MaterialPageRoute<dynamic>(
          builder: (context) => NetworkDevices(
              key: typedArgs.key, avaliableDevices: typedArgs.avaliableDevices),
          settings: settings,
        );
      case Routes.picodeScreen:
        if (hasInvalidArgs<PinCodeVerificationScreenArguments>(args)) {
          return misTypedArgsRoute<PinCodeVerificationScreenArguments>(args);
        }
        final typedArgs = args as PinCodeVerificationScreenArguments ??
            PinCodeVerificationScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (context) => PinCodeVerificationScreen(
              key: typedArgs.key,
              phoneNumber: typedArgs.phoneNumber,
              userGoogleInfo: typedArgs.userGoogleInfo),
          settings: settings,
        );
      case Routes.mobileVerification:
        if (hasInvalidArgs<MobileVerificationArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<MobileVerificationArguments>(args);
        }
        final typedArgs = args as MobileVerificationArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) => MobileVerification(
              key: typedArgs.key, userGoogleInfo: typedArgs.userGoogleInfo),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

// *************************************************************************
// Arguments holder classes
// **************************************************************************

//CwRegistration arguments holder class
class CwRegistrationArguments {
  final Key key;
  final UserGoogleInfo userGoogleInfo;
  final String phoneNumber;
  CwRegistrationArguments(
      {this.key, @required this.userGoogleInfo, @required this.phoneNumber});
}

//HomePage arguments holder class
class HomePageArguments {
  final Key key;
  final User user;
  HomePageArguments({this.key, @required this.user});
}

//NetworkDevices arguments holder class
class NetworkDevicesArguments {
  final Key key;
  final Map<String, int> avaliableDevices;
  NetworkDevicesArguments({this.key, this.avaliableDevices});
}

//PinCodeVerificationScreen arguments holder class
class PinCodeVerificationScreenArguments {
  final Key key;
  final String phoneNumber;
  final UserGoogleInfo userGoogleInfo;
  PinCodeVerificationScreenArguments(
      {this.key, this.phoneNumber, this.userGoogleInfo});
}

//MobileVerification arguments holder class
class MobileVerificationArguments {
  final Key key;
  final UserGoogleInfo userGoogleInfo;
  MobileVerificationArguments({this.key, @required this.userGoogleInfo});
}

// *************************************************************************
// Navigation helper methods extension
// **************************************************************************

extension RouterNavigationHelperMethods on ExtendedNavigatorState {
  Future pushSplashPage() => pushNamed(Routes.splashPage);

  Future pushCwGoogleSignIn() => pushNamed(Routes.cwGoogleSignIn);

  Future pushCwRegistration({
    Key key,
    @required UserGoogleInfo userGoogleInfo,
    @required String phoneNumber,
  }) =>
      pushNamed(
        Routes.cwRegistration,
        arguments: CwRegistrationArguments(
            key: key, userGoogleInfo: userGoogleInfo, phoneNumber: phoneNumber),
      );

  Future pushHomePage({
    Key key,
    @required User user,
  }) =>
      pushNamed(
        Routes.homePage,
        arguments: HomePageArguments(key: key, user: user),
      );

  Future pushNetworkDevices({
    Key key,
    Map<String, int> avaliableDevices,
  }) =>
      pushNamed(
        Routes.networkDevices,
        arguments: NetworkDevicesArguments(
            key: key, avaliableDevices: avaliableDevices),
      );

  Future pushPicodeScreen({
    Key key,
    String phoneNumber,
    UserGoogleInfo userGoogleInfo,
  }) =>
      pushNamed(
        Routes.picodeScreen,
        arguments: PinCodeVerificationScreenArguments(
            key: key, phoneNumber: phoneNumber, userGoogleInfo: userGoogleInfo),
      );

  Future pushMobileVerification({
    Key key,
    @required UserGoogleInfo userGoogleInfo,
  }) =>
      pushNamed(
        Routes.mobileVerification,
        arguments: MobileVerificationArguments(
            key: key, userGoogleInfo: userGoogleInfo),
      );
}
