import 'package:auto_route/auto_route_annotations.dart';
import 'package:cloudwalker_companion/presentation/cw_registration/cw_registration.dart';
import 'package:cloudwalker_companion/presentation/mobile_verification/pin_code_verfication_screen.dart';
import 'package:cloudwalker_companion/presentation/google_sign_in/cw_google_sign_in.dart';
import 'package:cloudwalker_companion/presentation/home/home_page.dart';
import 'package:cloudwalker_companion/presentation/mobile_verification/mobile_verification.dart';
import 'package:cloudwalker_companion/presentation/network_devices/network_devices.dart';
import 'package:cloudwalker_companion/presentation/splash/splash_page.dart';

@MaterialAutoRouter(generateNavigationHelperExtension: true)
class $Router {
  @initial
  SplashPage splashPage;
  CwGoogleSignIn cwGoogleSignIn;
  CwRegistration cwRegistration;
  HomePage homePage;
  NetworkDevices networkDevices;
  PinCodeVerificationScreen picodeScreen;
  MobileVerification mobileVerification;
}
