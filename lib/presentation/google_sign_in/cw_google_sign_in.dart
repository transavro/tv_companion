import 'package:auto_route/auto_route.dart';
import 'package:cloudwalker_companion/application/auth/auth_bloc.dart';
import 'package:cloudwalker_companion/presentation/core/widgets/sub_title.dart';
import 'package:cloudwalker_companion/presentation/core/widgets/title.dart';
import 'package:cloudwalker_companion/presentation/routes/router.gr.dart';
import 'package:flushbar/flushbar_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CwGoogleSignIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: BlocConsumer<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state is NoNetworkState) {
            FlushbarHelper.createError(
                    message: "No Internet", duration: Duration(seconds: 1))
                .show(context);
          } else if (state is AuthLoadingState) {
            FlushbarHelper.createLoading(
                    message: "Processing...",
                    duration: Duration(seconds: 1),
                    linearProgressIndicator: LinearProgressIndicator())
                .show(context);
          } else if (state is GoogleSignInCompletedState) {
            FlushbarHelper.createSuccess(
              message: "Google Sign In Successfully",
              duration: Duration(seconds: 1),
            ).show(context);
            ExtendedNavigator.of(context).pushReplacementNamed(
              Routes.mobileVerification,
              arguments: MobileVerificationArguments(
                  userGoogleInfo: state.userGoogleInfo),
            );
          } else if (state is AuthServerErrorState) {
            FlushbarHelper.createError(
              message: state.errorMessage,
              title: 'Server Error',
              duration: Duration(seconds: 3),
            ).show(context);
          } else if (state is CwSignInCompletedState) {
            FlushbarHelper.createSuccess(
              message: "Cloud Tv registered User found.",
              duration: Duration(seconds: 1),
            ).show(context).then((value) {
              ExtendedNavigator.of(context).pushReplacementNamed(
                  Routes.homePage,
                  arguments: HomePageArguments(user: state.user));
            });
          }
        },
        builder: (context, state) {
          return Center(
            child: Column(
              children: [
                const SizedBox(height: 90),
                buildTitle("Sign In"),
                const SizedBox(height: 40),
                buildSubtitle("Sign in or register via your Google"),
                buildSubtitle("account to create a CloudTV profile."),
                const SizedBox(height: 130),
                _buildGoogleSignInBtn(context),
              ],
            ),
          );
        },
      ),
    );
  }

  RaisedButton _buildGoogleSignInBtn(BuildContext context) {
    return RaisedButton(
      onPressed: () {
        context.bloc<AuthBloc>().add(GoogleSignInRequested());
      },
      elevation: 50,
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
      padding: EdgeInsets.fromLTRB(43, 13, 43, 13),
      color: Colors.blue,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            "assets/images/google_icon.png",
            width: 25,
            height: 25,
          ),
          SizedBox(width: 10),
          Text(
            "Sign in with Google",
            style: TextStyle(
              color: Colors.white,
              fontSize: 15,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
