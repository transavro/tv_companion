import 'package:flutter/material.dart';

class CwPrefrence extends StatefulWidget {
  final String tag;
  final Map<String, bool> targetMap;

  CwPrefrence({@required this.tag, @required this.targetMap});

  @override
  _CwPrefrenceState createState() => _CwPrefrenceState();
}

class _CwPrefrenceState extends State<CwPrefrence> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text("Choose ${widget.tag}"),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(
              Icons.check_circle,
              size: 30,
              color: Colors.blue[600],
            ),
            onPressed: () {
              Map<String, bool> result = Map();
              widget.targetMap.forEach((key, value) {
                if (value == true) {
                  result[key] = value;
                }
              });
              Navigator.of(context).pop(result);
            },
          ),
          SizedBox(width: 10)
        ],
      ),
      backgroundColor: Colors.black,
      body: Container(
        width: size.width,
        height: size.height,
        margin: EdgeInsets.fromLTRB(30, 40, 30, 0),
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            _buildPrefrenceList(),
          ],
        ),
      ),
    );
  }

  Widget _buildPrefrenceList() {
    return Expanded(
      child: ListView.separated(
          physics: const AlwaysScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemCount: widget.targetMap.length,
          shrinkWrap: true,
          separatorBuilder: (context, index) {
            return Divider(
              color: Colors.grey,
              height: 0.1,
            );
          },
          itemBuilder: (context, index) {
            String element = widget.targetMap.keys.elementAt(index);
            return CheckboxListTile(
              title: Text(
                element,
                style: TextStyle(color: Colors.white, fontSize: 13),
              ),
              activeColor: Colors.blue[800],
              value: widget.targetMap[element],
              onChanged: (value) {
                setState(() {
                  widget.targetMap[element] = value;
                });
              },
            );
          }),
    );
  }
}
