import 'package:auto_route/auto_route.dart';
import 'package:cloudwalker_companion/application/auth/auth_bloc.dart';
import 'package:cloudwalker_companion/application/tv_interaction/tv_interaction_bloc.dart';
import 'package:cloudwalker_companion/domain/cw_registration/models/protos/google/type/date.pb.dart';
import 'package:cloudwalker_companion/domain/google_sign_in/models/user_google_info.dart';
import 'package:flushbar/flushbar_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'widgets/always_disabled_focus_node.dart';
import 'widgets/cw_preferences.dart';
import 'package:cloudwalker_companion/domain/cw_registration/models/protos/User.pb.dart';
import 'package:cloudwalker_companion/presentation/routes/router.gr.dart';

class CwRegistration extends StatefulWidget {
  Map<String, bool> languagesCwMap = {
    "Arabic": false,
    "Assamese": false,
    "Bengali": false,
    "Czech": false,
    "Dutch": false,
    "English": false,
    "Flemish": false,
    "French": false,
    "German": false,
    "Gujarati": false,
    "Hebrew": false,
    "Hindi": false,
    "Japanese": false,
    "Kannada": false,
    "Khmer": false,
    "Korean": false,
    "Malayalam": false,
    "Mandarin": false,
    "Marathi": false,
    "Norwegian": false,
    "Persian": false,
    "Portuguese": false,
    "Punjabi": false,
    "Russian": false,
    "Sanskrit": false,
    "Spanish": false,
    "Tamil": false,
    "Telugu": false,
    "Turkish": false,
    "Urdu": false,
  };

  Map<String, bool> contentTypeCwMap = {
    "Apps": false,
    "Awards": false,
    "Cookery": false,
    "Devotional Videos": false,
    "Documentary": false,
    "Fitness": false,
    "Funny Videos": false,
    "Kids": false,
    "Motivational Videos": false,
    "Movie Trailers": false,
    "Movies": false,
    "Music ": false,
    "Popular Videos": false,
    "Series": false,
    "Short Film": false,
    "TalkShow": false,
    "Wild Life": false,
  };

  Map<String, bool> genreCwMap = {
    "Action": false,
    "Adventure": false,
    "Animation": false,
    "Art": false,
    "Arts And Artist": false,
    "Automobiles": false,
    "Biography": false,
    "Comedy": false,
    "Conspiracy": false,
    "Cookeryshow": false,
    "Crime": false,
    "Culture": false,
    "Dance": false,
    "Dark Comedy": false,
    "Devotional": false,
    "Disaster": false,
    "Documentary": false,
    "Drama": false,
    "Drugs": false,
    "Economics": false,
    "Education": false,
    "Entertainment": false,
    "Environment": false,
    "Family": false,
    "Fantasy": false,
    "Finance": false,
    "Game Show": false,
    "History": false,
    "Horror": false,
    "Infotainment": false,
    "Investigation": false,
    "Life": false,
    "Medical": false,
    "Military": false,
    "Music": false,
    "Musical": false,
    "Mystery": false,
    "Mythology": false,
    "Nature": false,
    "News": false,
    "People": false,
    "Police": false,
    "Politics": false,
    "Reality": false,
    "Religion": false,
    "Romance": false,
    "Science": false,
    "Sci-Fi": false,
    "Short": false,
    "Social": false,
    "Society": false,
    "Space": false,
    "Sports": false,
    "Standup Comedy": false,
    "Suspence": false,
    "Talk Show": false,
    "Technology": false,
    "Thriller": false,
    "Travel": false,
    "Visual": false,
    "War": false,
    "Western": false,
    "Wildlife": false,
  };

  final UserGoogleInfo userGoogleInfo;
  final String phoneNumber;

  CwRegistration(
      {Key key, @required this.userGoogleInfo, @required this.phoneNumber})
      : super(key: key);

  @override
  _CwRegistrationState createState() => _CwRegistrationState();
}

class _CwRegistrationState extends State<CwRegistration> {
  final _formKey = GlobalKey<FormState>();
  User user = User();
  DateTime _dateTime;
  List gender = ["Male", "Female", "Other"];
  String select;
  FocusNode _firstNameFocus = FocusNode();
  FocusNode _lastNameFocus = FocusNode();

  TextEditingController genreController = TextEditingController();
  TextEditingController languageController = TextEditingController();
  TextEditingController typeController = TextEditingController();
  TextEditingController dateOfBirthController = TextEditingController();

  @override
  void initState() {
    user.email = widget.userGoogleInfo.email;
    user.googleId = widget.userGoogleInfo.googleId;
    user.imageUrl = widget.userGoogleInfo.photoUrl;
    super.initState();
  }

  @override
  void dispose() {
    genreController.dispose();
    languageController.dispose();
    typeController.dispose();
    dateOfBirthController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: BlocConsumer<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state is NoNetworkState) {
            FlushbarHelper.createError(
                    message: "No Internet", duration: Duration(seconds: 1))
                .show(context);
          } else if (state is AuthServerErrorState) {
            FlushbarHelper.createError(
                    message: state.errorMessage, duration: Duration(seconds: 1))
                .show(context);
          } else if (state is AuthLoadingState) {
            FlushbarHelper.createLoading(
                    message: "Processing...",
                    duration: Duration(seconds: 1),
                    linearProgressIndicator: LinearProgressIndicator())
                .show(context);
          } else if (state is CwSignInCompletedState) {
            FlushbarHelper.createSuccess(
              message: "User Registered Successfully",
              duration: Duration(seconds: 1),
            ).show(context).whenComplete(() {
              context.bloc<TvInteractionBloc>()..add(GetAvaliableDevices());
              ExtendedNavigator.of(context).pushReplacementNamed(
                Routes.homePage,
                arguments: HomePageArguments(user: state.user),
              );
            });
          } else if (state is NoAuthState) {
            ExtendedNavigator.of(context)
                .pushReplacementNamed(Routes.cwGoogleSignIn);
          }
        },
        builder: (context, state) {
          return Form(
            key: _formKey,
            child: SingleChildScrollView(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: _formFileds(),
              ),
            ),
          );
        },
      ),
    );
  }

  List<Widget> _formFileds() {
    List<Widget> fields = List();
    fields.add(const SizedBox(height: 60));
    fields.add(_buildProfileIcon(context));
    fields.add(const SizedBox(height: 23));

    fields.add(Text("User Info",
        style: TextStyle(color: Colors.blue[600], fontSize: 12)));
    fields.add(const SizedBox(height: 13));
    fields.add(_buildFirstNameField(context));
    fields.add(const SizedBox(height: 13));
    fields.add(_buildLastNameField(context));
    fields.add(const SizedBox(height: 13));
    fields.add(_buildEmailField());
    fields.add(const SizedBox(height: 13));
    fields.add(_buildPhoneNumberField(context));
    fields.add(const SizedBox(height: 13));
    fields.add(_buildDateOfBirthField(context));
    fields.add(const SizedBox(height: 13));
    fields.add(_buildGenderRadio(context));
    fields.add(const SizedBox(height: 13));
    fields.add(Text("User Preferences",
        style: TextStyle(color: Colors.blue[600], fontSize: 12)));
    fields.add(const SizedBox(height: 13));
    fields.add(_buildGenreField(context));
    fields.add(const SizedBox(height: 13));
    fields.add(_buildLanguageField(context));
    fields.add(const SizedBox(height: 13));
    fields.add(_buildContentTypeField(context));
    fields.add(const SizedBox(height: 13));

    fields.add(const SizedBox(height: 40));
    fields.add(_buildNextButton(context));
    fields.add(const SizedBox(height: 30));
    return fields;
  }

  TextFormField _buildDateOfBirthField(BuildContext context) {
    return TextFormField(
        controller: dateOfBirthController,
        keyboardType: TextInputType.datetime,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.cake),
          labelText: 'Date of Birth',
        ),
        autocorrect: false,
        style: TextStyle(fontSize: 12),
        enableInteractiveSelection: false,
        focusNode: AlwaysDisabledFocusNode(),
        onTap: () {
          // To dismiss the keyboard
          _firstNameFocus.unfocus();
          _lastNameFocus.unfocus();
          showDatePicker(
                  context: context,
                  initialDate: _dateTime == null ? DateTime.now() : _dateTime,
                  firstDate: DateTime(1965),
                  lastDate: DateTime(2030))
              .then((date) {
            setState(() {
              _dateTime = date;
              dateOfBirthController.text =
                  "${date.day}-${date.month}-${date.year}";
            });
          });
        },
        validator: (String input) {
          if (input.isEmpty) {
            return "Field cannot be empty";
          } else if (input.split("-").length != 3) {
            return "Invalid Date";
          } else if (int.parse(input.split("-")[0]) > 2005) {
            return "Age should be greater than 15";
          }
          return null;
        },
        onSaved: (value) {
          List<String> tmp = value.split('-');
          Date pbDate = Date();
          pbDate.day = int.parse(tmp[0]);
          pbDate.month = int.parse(tmp[1]);
          pbDate.year = int.parse(tmp[2]);
          user.dateOfBirth = pbDate;
        });
  }

  TextFormField _buildGenreField(BuildContext context) {
    return TextFormField(
      controller: genreController,
      enableInteractiveSelection: false,
      focusNode: AlwaysDisabledFocusNode(),
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.movie_filter),
        labelText: 'Genres',
      ),
      onTap: () async {
        // To dismiss the keyboard
        _firstNameFocus.unfocus();
        _lastNameFocus.unfocus();
        Map result = await Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) {
          return CwPrefrence(
            tag: "Genres",
            targetMap: widget.genreCwMap,
          );
        }));

        if (result != null && result.length > 0) {
          user.genre
            ..clear()
            ..addAll(result.keys);
          setState(() {
            genreController.text = result.keys.join(", ").toString();
          });
        }
      },
      autocorrect: false,
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        if (input.isEmpty) {
          return "Field cannot be empty";
        } else if (input.split(',').length < 4) {
          return "Select atleast 4 Genres";
        }
        return null;
      },
      onSaved: (value) => {},
    );
  }

  TextFormField _buildContentTypeField(BuildContext context) {
    return TextFormField(
      controller: typeController,
      enableInteractiveSelection: false,
      focusNode: AlwaysDisabledFocusNode(),
      onTap: () async {
        // To dismiss the keyboard
        _firstNameFocus.unfocus();
        _lastNameFocus.unfocus();
        Map result = await Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) {
          return CwPrefrence(
            tag: "Content Types",
            targetMap: widget.contentTypeCwMap,
          );
        }));

        if (result != null && result.length > 0) {
          user.contentType
            ..clear()
            ..addAll(result.keys);
          setState(() {
            typeController.text = result.keys.join(", ").toString();
          });
        }
      },
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.movie_creation),
        labelText: 'Content Types',
      ),
      autocorrect: false,
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        if (input.isEmpty) {
          return "Field cannot be empty";
        } else if (input.split(',').length < 4) {
          return "Select atleast 4 Content Types";
        }
        return null;
      },
      onSaved: (value) => {},
    );
  }

  TextFormField _buildLanguageField(BuildContext context) {
    return TextFormField(
      controller: languageController,
      enableInteractiveSelection: false,
      focusNode: AlwaysDisabledFocusNode(),
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.language),
        labelText: 'Languages',
      ),
      autocorrect: false,
      onTap: () async {
        // To dismiss the keyboard
        _firstNameFocus.unfocus();
        _lastNameFocus.unfocus();
        Map result = await Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) {
          return CwPrefrence(
            tag: "Languages",
            targetMap: widget.languagesCwMap,
          );
        }));

        if (result != null && result.length > 0) {
          user.language
            ..clear()
            ..addAll(result.keys);
          setState(() {
            languageController.text = result.keys.join(", ").toString();
          });
        }
      },
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        if (input.isEmpty) {
          return "Field cannot be empty";
        } else if (input.split(',').length < 4) {
          return "Select atleast 4 Languages";
        }
        return null;
      },
      onSaved: (value) => {},
    );
  }

  TextFormField _buildEmailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.email),
        labelText: 'Email',
      ),
      enabled: false,
      initialValue: user.email,
      autocorrect: false,
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        if (input.isEmpty) {
          return "Field cannot be empty";
        }
        return null;
      },
      onSaved: (value) => user.email,
    );
  }

  TextFormField _buildFirstNameField(BuildContext context) {
    return TextFormField(
      focusNode: _firstNameFocus,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.person_pin),
        labelText: 'First Name',
      ),
      autocorrect: false,
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        Pattern pattern = r'^[A-Za-z]+(?:[ _-][A-Za-z]+)*$';
        RegExp regex = new RegExp(pattern);
        if (input.isEmpty) {
          return "Field cannot be empty";
        } else if (input.length < 3) {
          return "First name should be 3 characters long";
        } else if (!regex.hasMatch(input))
          return 'Invalid First name';
        else
          return null;
      },
      autofocus: false,
      onSaved: (value) => user.name = value,
    );
  }

  TextFormField _buildLastNameField(BuildContext context) {
    return TextFormField(
      focusNode: _lastNameFocus,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.person_pin),
        labelText: 'Last Name',
      ),
      autocorrect: false,
      autofocus: false,
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        Pattern pattern = r'^[A-Za-z]+(?:[ _-][A-Za-z]+)*$';
        RegExp regex = new RegExp(pattern);
        if (input.isEmpty) {
          return "Field cannot be empty";
        } else if (input.length < 3) {
          return "Last name should be 3 characters long";
        } else if (!regex.hasMatch(input))
          return 'Invalid Last name';
        else
          return null;
      },
      onSaved: (value) {
        user.name = user.name + " " + value;
      },
    );
  }

  TextFormField _buildPhoneNumberField(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.phone),
        labelText: 'Phone Number',
      ),
      initialValue: widget.phoneNumber,
      enabled: false,
      autocorrect: false,
      style: TextStyle(fontSize: 12),
      validator: (String input) {
        if (input.isEmpty) {
          return "Field cannot be empty";
        }
        return null;
      },
      onSaved: (value) => user.phoneNumber = value,
    );
  }

  Row addRadioButton(int btnValue, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          activeColor: Theme.of(context).primaryColor,
          value: gender[btnValue],
          groupValue: select,
          onChanged: (value) {
            // To dismiss the keyboard
            _firstNameFocus.unfocus();
            _lastNameFocus.unfocus();
            setState(() {
              print(value);
              select = value;
              if (value == "Male") {
                user.gender = Gender.MALE;
              } else if (value == "Female") {
                user.gender = Gender.FEMALE;
              } else {
                user.gender = Gender.NOTDEFINED;
              }
            });
          },
        ),
        Text(title, style: TextStyle(fontSize: 12))
      ],
    );
  }

  Widget _buildGenderRadio(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Gender", style: TextStyle(fontSize: 12, color: Colors.blue[600])),
        Row(
          children: <Widget>[
            addRadioButton(0, 'Male'),
            addRadioButton(1, 'Female'),
            addRadioButton(2, 'Others'),
          ],
        )
      ],
    );
  }

  Widget _buildProfileIcon(BuildContext context) {
    return Center(
        child: CircleAvatar(
      radius: 40.0,
      backgroundImage: NetworkImage(user.imageUrl),
      backgroundColor: Colors.transparent,
    ));
  }

  Widget _buildNextButton(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Center(
      child: InkWell(
        onTap: () {
          if (_formKey.currentState.validate()) {
            //saving all the values
            _formKey.currentState.save();
            context.bloc<AuthBloc>().add(CwSignInRequested(user));
          } else {
            FlushbarHelper.createError(
                    title: "Form Invalid",
                    message: "Invalid Form. Please check the fields.",
                    duration: Duration(seconds: 1))
                .show(context);
          }
        },
        child: Container(
          padding: EdgeInsets.all(2),
          width: size.height * 0.10,
          height: size.height * 0.10,
          decoration: new BoxDecoration(
            color: Colors.blue[800],
            shape: BoxShape.circle,
          ),
          child: Icon(
            Icons.chevron_right,
            color: Colors.white,
            size: 38,
          ),
        ),
      ),
    );
  }
}
