import 'package:auto_route/auto_route.dart';
import 'package:cloudwalker_companion/application/auth/auth_bloc.dart';
import 'package:cloudwalker_companion/presentation/routes/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBloc, AuthState>(
      listener: (context, state) {
        if (state is NoAuthState) {
          ExtendedNavigator.of(context)
              .pushReplacementNamed(Routes.cwGoogleSignIn);
        } else if (state is OnlyGoogleSignedInState) {
          ExtendedNavigator.of(context).pushReplacementNamed(
            Routes.mobileVerification,
            arguments: MobileVerificationArguments(
                userGoogleInfo: state.userGoogleInfo),
          );
        } else if (state is CwSignInCompletedState) {
          ExtendedNavigator.of(context).pushReplacementNamed(Routes.homePage,
              arguments: HomePageArguments(user: state.user));
        }
      },
      child: _buildSplashScreen(),
    );
  }

  Widget _buildSplashScreen() {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/brand.png',
            width: 250,
            height: 400,
          ),
          CircularProgressIndicator(),
        ],
      ),
    );
  }
}
