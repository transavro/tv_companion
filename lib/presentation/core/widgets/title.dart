import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

Widget buildTitle(String data) {
  return Text(
    data,
    style: TextStyle(
      color: Colors.white,
      fontSize: 20.0,
    ),
  );
}
