import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

Widget buildSubtitle(String data) {
  return Text(
    data,
    style: TextStyle(
      color: Colors.grey,
      fontSize: 12.0,
    ),
  );
}
