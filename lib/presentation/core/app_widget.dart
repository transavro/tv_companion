import 'package:auto_route/auto_route.dart';
import 'package:cloudwalker_companion/application/all_apps/all_apps_bloc.dart';
import 'package:cloudwalker_companion/application/auth/auth_bloc.dart';
import 'package:cloudwalker_companion/application/tv_interaction/tv_interaction_bloc.dart';
import 'package:cloudwalker_companion/presentation/routes/router.gr.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../injection.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthBloc>(
          create: (context) => getIt<AuthBloc>()..add(AuthCheckRequested()),
        ),
        BlocProvider<TvInteractionBloc>(
          create: (context) =>
              getIt<TvInteractionBloc>()..add(GetAvaliableDevices()),
        ),
        BlocProvider<AllAppsBloc>(
          create: (context) => getIt<AllAppsBloc>(),
        ),
      ],
      child: MaterialApp(
        title: 'Tv Companion',
        debugShowCheckedModeBanner: false,
        builder: ExtendedNavigator(router: Router()),
        theme: ThemeData.dark().copyWith(
          primaryColor: Colors.blue[800],
          scaffoldBackgroundColor: Colors.black,
          backgroundColor: Colors.black,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          accentColor: Colors.blueAccent,
          // inputDecorationTheme: InputDecorationTheme(
          //     border: OutlineInputBorder(
          //   borderRadius: BorderRadius.circular(30),
          // )),
        ),
      ),
    );
  }
}
