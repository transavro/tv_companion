// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:cloudwalker_companion/infrastructure/core/app_injectable_module.dart';
import 'package:cloudwalker_companion/domain/search/models/protos/CDEService.pbgrpc.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:cloudwalker_companion/infrastructure/tv_interaction/tv_interaction_facade_impl.dart';
import 'package:cloudwalker_companion/domain/tv_interaction/i_tv_interaction_facade.dart';
import 'package:multicast_dns/multicast_dns.dart';
import 'package:cloudwalker_companion/domain/cw_registration/models/protos/User.pbgrpc.dart';
import 'package:cloudwalker_companion/application/all_apps/all_apps_bloc.dart';
import 'package:cloudwalker_companion/infrastructure/auth/auth_registration_facade_impl.dart';
import 'package:cloudwalker_companion/domain/auth/i_auth_registration_facade.dart';
import 'package:cloudwalker_companion/application/tv_interaction/tv_interaction_bloc.dart';
import 'package:cloudwalker_companion/application/auth/auth_bloc.dart';
import 'package:get_it/get_it.dart';

void $initGetIt(GetIt g, {String environment}) {
  final appInjectableModule = _$AppInjectableModule();
  g.registerLazySingleton<CDEServiceClient>(
      () => appInjectableModule.cdeServiceClient);
  g.registerLazySingleton<GoogleSignIn>(() => appInjectableModule.googleSignIn);
  g.registerLazySingleton<ITvInteractionFacade>(
      () => TvInteractionFacadeImpl());
  g.registerLazySingleton<MDnsClient>(() => appInjectableModule.mdnsClient);
  g.registerLazySingleton<UserServiceClient>(
      () => appInjectableModule.userServiceClient);
  g.registerFactory<AllAppsBloc>(() => AllAppsBloc(g<ITvInteractionFacade>()));
  g.registerLazySingleton<IAuthRegistrationFacade>(() =>
      AuthRegistrationFacadeImpl(g<UserServiceClient>(), g<GoogleSignIn>()));
  g.registerFactory<TvInteractionBloc>(() => TvInteractionBloc(
        g<MDnsClient>(),
        g<ITvInteractionFacade>(),
        g<IAuthRegistrationFacade>(),
      ));
  g.registerFactory<AuthBloc>(() => AuthBloc(g<IAuthRegistrationFacade>()));
}

class _$AppInjectableModule extends AppInjectableModule {}
