part of 'tv_interaction_bloc.dart';

abstract class TvInteractionEvent extends Equatable {
  const TvInteractionEvent();
}

class GetAvaliableDevices extends TvInteractionEvent {
  @override
  List<Object> get props => [];
}

class LinkAndConnectDevice extends TvInteractionEvent {
  final String ip;
  final int port;

  LinkAndConnectDevice(this.ip, this.port);

  @override
  List<Object> get props => [ip, port];
}

class DeleteLinkedDevice extends TvInteractionEvent {
  @override
  List<Object> get props => [];
}

class GetAllAppsEvent extends TvInteractionEvent {
  @override
  List<Object> get props => [];
}

class TvKeyCodeEvent extends TvInteractionEvent {
  final String keycode;
  TvKeyCodeEvent(this.keycode);

  @override
  List<Object> get props => [keycode];
}

class TvAppActionEvent extends TvInteractionEvent {
  final String packageName;

  TvAppActionEvent(this.packageName);
  @override
  List<Object> get props => [packageName];
}

class TvSourcesEvent extends TvInteractionEvent {
  @override
  List<Object> get props => [];
}
