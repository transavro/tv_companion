part of 'tv_interaction_bloc.dart';

abstract class TvInteractionState extends Equatable {
  const TvInteractionState();
}

class TvInteractionInitial extends TvInteractionState {
  @override
  List<Object> get props => [];
}

class TvInteractionLoading extends TvInteractionState {
  @override
  List<Object> get props => [];
}

class NoWifiState extends TvInteractionState {
  @override
  List<Object> get props => [];
}

class TvInteractionErrorState extends TvInteractionState {
  final String message;

  TvInteractionErrorState(this.message);
  @override
  List<Object> get props => [message];
}

class NewTvDeviceState extends TvInteractionState {
  final String domainName;
  final int port;

  NewTvDeviceState(this.domainName, this.port);

  @override
  List<Object> get props => [domainName, port];
}

class AvaliableDevicesState extends TvInteractionState {
  final Map<String, int> devices;
  AvaliableDevicesState(this.devices);
  @override
  List<Object> get props => [devices];
}

class NoDevicesAvaliableState extends TvInteractionState {
  @override
  List<Object> get props => [];
}

class TvSuccesufullyConnectedState extends TvInteractionState {
  final String linkedTvName;
  TvSuccesufullyConnectedState(this.linkedTvName);
  @override
  List<Object> get props => [linkedTvName];
}

class AllAppsLoadingState extends TvInteractionState {
  AllAppsLoadingState();
  @override
  List<Object> get props => [];
}

class AllAppsState extends TvInteractionState {
  final GetAllAppsResp appsResp;
  AllAppsState(this.appsResp);
  @override
  List<Object> get props => [this.appsResp];
}

class TvSourcesState extends TvInteractionState {
  final List<String> tvSources;

  TvSourcesState(this.tvSources);

  @override
  List<Object> get props => [this.tvSources];
}
