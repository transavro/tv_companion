import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloudwalker_companion/domain/auth/i_auth_registration_facade.dart';
import 'package:cloudwalker_companion/domain/core/exceptions/device_already_link_exception.dart';
import 'package:cloudwalker_companion/domain/core/exceptions/no_network_exception.dart';
import 'package:cloudwalker_companion/domain/cw_registration/models/protos/User.pbgrpc.dart';
import 'package:cloudwalker_companion/domain/tv_interaction/exceptions/no_wifi_exception.dart';
import 'package:cloudwalker_companion/domain/tv_interaction/i_tv_interaction_facade.dart';
import 'package:cloudwalker_companion/domain/tv_interaction/models/protos/TvInteraction.pbgrpc.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:multicast_dns/multicast_dns.dart';

part 'tv_interaction_event.dart';
part 'tv_interaction_state.dart';

@injectable
class TvInteractionBloc extends Bloc<TvInteractionEvent, TvInteractionState> {
  final String name = '_http._tcp.local';
  final MDnsClient _mdsClient;
  final ITvInteractionFacade _iTvInteractionFacade;
  final IAuthRegistrationFacade _iAuthRegistrationFacade;

  TvInteractionBloc(this._mdsClient, this._iTvInteractionFacade,
      this._iAuthRegistrationFacade);

  @override
  TvInteractionState get initialState => TvInteractionInitial();

  @override
  Stream<TvInteractionState> mapEventToState(
    TvInteractionEvent event,
  ) async* {
    if (event is GetAvaliableDevices) {
      yield TvInteractionLoading();
      final resultMap = Map<String, int>();
      await _mdsClient.start();
      await for (PtrResourceRecord ptr in _mdsClient
          .lookup<PtrResourceRecord>(ResourceRecordQuery.serverPointer(name))) {
        await for (SrvResourceRecord srv
            in _mdsClient.lookup<SrvResourceRecord>(
                ResourceRecordQuery.service(ptr.domainName))) {
          if (!resultMap.containsKey(ptr.domainName)) {
            final String bundleId = ptr.domainName;
            resultMap[bundleId] = srv.port;
          }
        }
      }
      _mdsClient.stop();
      if (resultMap.length == 0) {
        yield NoDevicesAvaliableState();
      } else {
        yield AvaliableDevicesState(resultMap);
      }
    } else if (event is LinkAndConnectDevice) {
      try {
        await _iTvInteractionFacade.connectToTv(event.ip, event.port);
        final tvDataResp =
            await _iTvInteractionFacade.getTvData("mobileDevice");
        yield TvSuccesufullyConnectedState(tvDataResp.data["Tv Model"]);
        LinkedDevice linkedDevice = LinkedDevice();
        linkedDevice.tvEmac = tvDataResp.data["Tv Emac"];
        linkedDevice.tvBoard = tvDataResp.data["Tv Board"];
        linkedDevice.tvPanel = tvDataResp.data["Tv Panel"];
        linkedDevice.tvName = tvDataResp.data["Tv Model"];
        await _iAuthRegistrationFacade.addLinkedDevice(linkedDevice);
      } catch (e) {
        if (e is NoWifiException) {
          yield NoWifiState();
        } else if (e is NoNetworkException) {
          yield TvInteractionErrorState("No Connected to Internet");
        } else if (e is DeviceAlreadyLinkedException) {
          yield TvInteractionErrorState("Device Already linked");
        } else {
          yield TvInteractionErrorState(e.toString());
        }
      }
    } else if (event is GetAllAppsEvent) {
      try {
        yield AllAppsLoadingState();
        final result =
            await _iTvInteractionFacade.getAllTvApps(GetAllAppsReq());
        yield AllAppsState(result);
      } catch (e) {
        if (e is NoWifiException) {
          yield NoWifiState();
        } else {
          yield TvInteractionErrorState(e.toString());
        }
      }
    } else if (event is TvKeyCodeEvent) {
      try {
        await _iTvInteractionFacade
            .keycodeTrigger(KeycodeReq()..keycode = event.keycode);
      } catch (e) {
        print("#CW KEYCODE ERROR $e");
        if (e is NoWifiException) {
          yield NoWifiState();
        } else {
          yield TvInteractionErrorState(e.toString());
        }
      }
    } else if (event is TvAppActionEvent) {
      try {
        await _iTvInteractionFacade.appActionTrigger(AppReq()
          ..app = App.APP_OPEN
          ..package = event.packageName);
      } catch (e) {
        if (e is NoWifiException) {
          yield NoWifiState();
        } else {
          yield TvInteractionErrorState(e);
        }
      }
    } else if (event is TvSourcesEvent) {
      try {
        List<String> tvSources = await _iTvInteractionFacade.getTvSources();
        yield TvSourcesState(tvSources);
      } catch (e) {
        if (e is NoWifiException) {
          yield NoWifiState();
        } else {
          yield TvInteractionErrorState(e);
        }
      }
    }
  }
}
