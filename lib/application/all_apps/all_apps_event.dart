part of 'all_apps_bloc.dart';

abstract class AllAppsEvent extends Equatable {
  const AllAppsEvent();
}

class GetAllAppsEvent extends AllAppsEvent {
  @override
  List<Object> get props => [];
}

class AppActionEvent extends AllAppsEvent {
  final AppReq appReq;

  AppActionEvent(this.appReq);

  @override
  List<Object> get props => [this.appReq];
}
