part of 'all_apps_bloc.dart';

abstract class AllAppsState extends Equatable {
  const AllAppsState();
}

class AllAppsLoading extends AllAppsState{
  @override
  List<Object> get props => [];
}

class AllActionLoading extends AllAppsState{
  @override
  List<Object> get props => [];
}


class NoWifiState extends AllAppsState {
  @override
  List<Object> get props => [];
}

class AppInteractionErrorState extends AllAppsState {
  final String message;

  AppInteractionErrorState(this.message);
  @override
  List<Object> get props => [message];
}

class AllAppsLoaded extends AllAppsState{
  final GetAllAppsResp resp;

  AllAppsLoaded(this.resp);
  @override
  List<Object> get props => [resp];

}

class AllAppsInitial extends AllAppsState {
  @override
  List<Object> get props => [];
}
