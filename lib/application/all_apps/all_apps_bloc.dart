import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloudwalker_companion/domain/tv_interaction/exceptions/no_wifi_exception.dart';
import 'package:cloudwalker_companion/domain/tv_interaction/i_tv_interaction_facade.dart';
import 'package:cloudwalker_companion/domain/tv_interaction/models/protos/TvInteraction.pbgrpc.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';

part 'all_apps_event.dart';
part 'all_apps_state.dart';


@injectable
class AllAppsBloc extends Bloc<AllAppsEvent, AllAppsState> {

  final ITvInteractionFacade _iTvInteractionFacade;

  AllAppsBloc(this._iTvInteractionFacade);

  @override
  AllAppsState get initialState => AllAppsInitial();

  @override
  Stream<AllAppsState> mapEventToState(
    AllAppsEvent event,
  ) async* {
    if(event is GetAllAppsEvent)
    {
      yield AllAppsLoading();
      try
      {
        yield AllAppsLoaded(await _iTvInteractionFacade.getAllTvApps(GetAllAppsReq()));
      } 
      catch (e)
      {
        if (e is NoWifiException) {
          yield NoWifiState();
        } else {
          yield AppInteractionErrorState(e);
        }
      }  
    }
    else if(event is AppActionEvent)
    {
      try{
        await _iTvInteractionFacade.appActionTrigger(event.appReq);
      } 
      catch (e){
        if (e is NoWifiException) {
          yield NoWifiState();
        } else {
          yield AppInteractionErrorState(e);
        }
      }
    }
  }
}
