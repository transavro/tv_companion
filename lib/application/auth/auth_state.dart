part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();
}

class AuthInitialState extends AuthState {
  @override
  List<Object> get props => [];
}

class AuthLoadingState extends AuthState {
  @override
  List<Object> get props => [];
}

class GoogleSignInCompletedState extends AuthState {
  final UserGoogleInfo userGoogleInfo;

  GoogleSignInCompletedState(this.userGoogleInfo);
  @override
  List<Object> get props => [userGoogleInfo];
}

class CwSignInCompletedState extends AuthState {
  final User user;

  CwSignInCompletedState(this.user);

  @override
  List<Object> get props => [user];
}

class NoAuthState extends AuthState {
  @override
  List<Object> get props => [];
}

class OnlyGoogleSignedInState extends AuthState {
  final UserGoogleInfo userGoogleInfo;

  OnlyGoogleSignedInState(this.userGoogleInfo);
  @override
  List<Object> get props => [userGoogleInfo];
}

class NoNetworkState extends AuthState {
  @override
  List<Object> get props => [];
}

class AuthServerErrorState extends AuthState {
  final String errorMessage;

  AuthServerErrorState(this.errorMessage);
  @override
  List<Object> get props => [errorMessage];
}

class UpdateSuccessfulState extends AuthState {
  final User user;

  UpdateSuccessfulState(this.user);
  @override
  List<Object> get props => [user];
}

class RemovedTvDeviceSuccessfullyState extends AuthState{
  @override
  List<Object> get props => [];
}