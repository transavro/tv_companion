part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();
}

class AuthCheckRequested extends AuthEvent{
  @override
  List<Object> get props => [];
}

class GoogleSignInRequested extends AuthEvent{
  @override
  List<Object> get props => [];
}

class CwSignInRequested extends AuthEvent{
  final User user;

  CwSignInRequested(this.user);
  @override
  List<Object> get props => [user];
}

class DeleteRegisteredUser extends AuthEvent{
  final String googleId;

  DeleteRegisteredUser(this.googleId);
  @override
  List<Object> get props => [googleId];
} 


class UpdateCurrentUser extends AuthEvent{
  final User user;

  UpdateCurrentUser(this.user);
  @override
  List<Object> get props => [user];
}

class RemoveTvDevice extends AuthEvent{
  final String tvEmac;

  RemoveTvDevice(this.tvEmac);

  @override
  List<Object> get props => [this.tvEmac];

}
