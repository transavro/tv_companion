import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloudwalker_companion/domain/auth/i_auth_registration_facade.dart';
import 'package:cloudwalker_companion/domain/core/exceptions/no_cw_signin_exception.dart';
import 'package:cloudwalker_companion/domain/core/exceptions/no_google_signin_exception.dart';
import 'package:cloudwalker_companion/domain/core/exceptions/no_network_exception.dart';
import 'package:cloudwalker_companion/domain/core/exceptions/server_error_exception.dart';
import 'package:cloudwalker_companion/domain/cw_registration/models/protos/User.pb.dart';
import 'package:cloudwalker_companion/domain/google_sign_in/models/user_google_info.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';

part 'auth_event.dart';
part 'auth_state.dart';

@injectable
class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final IAuthRegistrationFacade _iAuthRegFacade;

  AuthBloc(this._iAuthRegFacade);

  @override
  AuthState get initialState => AuthInitialState();

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    if (event is AuthCheckRequested) {
      yield AuthLoadingState();
      try {
        final user = await _iAuthRegFacade.getLogedInUser();
        yield CwSignInCompletedState(user);
      } catch (e) {
        if (e is NoGoogleSignInException) {
          yield NoAuthState();
        } else if (e is NoNetworkException) {
          yield NoNetworkState();
        } else if (e is NoCwSignInException) {
          yield OnlyGoogleSignedInState(e.userGoogleInfo);
        } else if (e is ServerErrorException) {
          yield AuthServerErrorState(e.toString());
        } else {}
      }
    } else if (event is GoogleSignInRequested) {
      yield AuthLoadingState();
      try {
        final result = await _iAuthRegFacade.signInWithGoogle();
        try {
          final user = await _iAuthRegFacade.getUser(result.googleId);
          yield CwSignInCompletedState(user);
        } catch (e) {
          yield GoogleSignInCompletedState(result);
        }
      } catch (e) {
        if (e is NoNetworkException) {
          yield NoNetworkState();
        } else if (e is ServerErrorException) {
          yield AuthServerErrorState(e.toString());
        }
      }
    } else if (event is CwSignInRequested) {
      yield AuthLoadingState();
      try {
        final user = await _iAuthRegFacade.createUser(event.user);
        yield CwSignInCompletedState(user);
      } catch (e) {
        if (e is NoGoogleSignInException) {
          yield NoAuthState();
        } else if (e is NoNetworkException) {
          yield NoNetworkState();
        } else if (e is NoCwSignInException) {
          yield OnlyGoogleSignedInState(e.userGoogleInfo);
        } else if (e is ServerErrorException) {
          yield AuthServerErrorState(e.toString());
        }
      }
    } else if (event is DeleteRegisteredUser) {
      yield AuthLoadingState();
      try {
        final result = await _iAuthRegFacade.deleteUser(event.googleId);
        if (result) {
          yield NoAuthState();
        } else {
          yield AuthServerErrorState("Not able to delete user.");
        }
      } catch (e) {
        if (e is NoGoogleSignInException) {
          yield NoAuthState();
        } else if (e is NoNetworkException) {
          yield NoNetworkState();
        } else if (e is NoCwSignInException) {
          yield OnlyGoogleSignedInState(e.userGoogleInfo);
        } else if (e is ServerErrorException) {
          yield AuthServerErrorState(e.toString());
        } else {
          yield AuthServerErrorState(e);
        }
      }
    } else if (event is UpdateCurrentUser) {
      yield AuthLoadingState();
      try {
        final result = await _iAuthRegFacade.updateUser(event.user);
        yield UpdateSuccessfulState(result);
      } catch (e) {
        if (e is NoGoogleSignInException) {
          yield NoAuthState();
        } else if (e is NoNetworkException) {
          yield NoNetworkState();
        } else if (e is NoCwSignInException) {
          yield OnlyGoogleSignedInState(e.userGoogleInfo);
        } else if (e is ServerErrorException) {
          yield AuthServerErrorState(e.toString());
        } else {
          yield AuthServerErrorState(e);
        }
      }
    } else if (event is RemoveTvDevice) {
      try {
        await _iAuthRegFacade.removeLinkedDevice(event.tvEmac);
        yield RemovedTvDeviceSuccessfullyState();
      } catch (e) {
        if (e is NoGoogleSignInException) {
          yield NoAuthState();
        } else if (e is NoNetworkException) {
          yield NoNetworkState();
        } else if (e is NoCwSignInException) {
          yield OnlyGoogleSignedInState(e.userGoogleInfo);
        } else if (e is ServerErrorException) {
          yield AuthServerErrorState(e.toString());
        } else {
          yield AuthServerErrorState(e);
        }
      }
    }
  }
}
