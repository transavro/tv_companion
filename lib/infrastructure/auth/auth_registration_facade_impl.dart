import 'package:cloudwalker_companion/domain/auth/i_auth_registration_facade.dart';
import 'package:cloudwalker_companion/domain/core/exceptions/no_cw_signin_exception.dart';
import 'package:cloudwalker_companion/domain/core/exceptions/no_google_signin_exception.dart';
import 'package:cloudwalker_companion/domain/core/exceptions/no_network_exception.dart';
import 'package:cloudwalker_companion/domain/core/exceptions/server_error_exception.dart';
import 'package:cloudwalker_companion/domain/cw_registration/models/protos/User.pbgrpc.dart';
import 'package:cloudwalker_companion/domain/google_sign_in/models/user_google_info.dart';
import 'package:cloudwalker_companion/domain/cw_registration/models/protos/User.pb.dart';
import 'package:cloudwalker_companion/infrastructure/core/app_utils.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:grpc/grpc.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@LazySingleton(as: IAuthRegistrationFacade)
class AuthRegistrationFacadeImpl implements IAuthRegistrationFacade {
  final UserServiceClient _serviceClient;
  final GoogleSignIn _googleSignIn;

  AuthRegistrationFacadeImpl(this._serviceClient, this._googleSignIn);

  @override
  Future<UserGoogleInfo> signInWithGoogle() async {
    if (!await checkConnection()) {
      throw NoNetworkException();
    } else {
      try {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        final googleUser = await _googleSignIn.signIn();
        if (googleUser == null) {
          prefs.setBool('isGoogleSignIn', false);
          throw ServerErrorException("Cancelled by user.");
        }

        prefs.setBool('isGoogleSignIn', true);
        prefs.setString("googleId", googleUser.id);
        prefs.setString("email", googleUser.email);
        prefs.setString("imageUrl", googleUser.photoUrl);
        return UserGoogleInfo(
            googleUser.email, googleUser.id, googleUser.photoUrl);
      } catch (e) {
        throw ServerErrorException(e.message);
      }
    }
  }

  @override
  Future<User> getLogedInUser() async {
    // check the status of google SignIn and Cw signIn
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final isGoogleSignIn = prefs.getBool("isGoogleSignIn");
    final isCwSignIn = prefs.getBool("isCwSignIn");
    final isUserLocallyPresent = prefs.getBool("isUserLocallyPresent");

    // checking google sign In
    if (isGoogleSignIn != null && isGoogleSignIn) {
      // checking cw sign in
      if (isCwSignIn != null && isCwSignIn) {
        // fetch user stored locally
        if (isUserLocallyPresent != null && isUserLocallyPresent) {
          final user = await getUserFromPrefs();
          return user;
        } else {
          // is user not found locally fetch from server
          try {
            return await getUser(prefs.getString("googleId"));
          } catch (e) {
            throw e;
          }
        }
      } else {
        throw NoCwSignInException(
          UserGoogleInfo(
            prefs.getString("email"),
            prefs.getString("googleId"),
            prefs.getString("imageUrl"),
          ),
        );
      }
    } else {
      // user is not google Sign In.
      throw NoGoogleSignInException();
    }
  }

  @override
  Future<User> createUser(User user) async {
    if (!await checkConnection()) {
      throw NoNetworkException();
    } else {
      try {
        await _serviceClient.addUser(
          user,
          options: CallOptions(
            timeout: Duration(seconds: 3),
          ),
        );
        await storeUserToPrefs(user);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setBool("isCwSignIn", true);
        return user;
      } catch (e) {
        if (e is GrpcError && e.message == "User already present") {
          await storeUserToPrefs(user);
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setBool("isCwSignIn", true);
          return user;
        }
        throw ServerErrorException(e.message);
      }
    }
  }

  @override
  Future<bool> deleteUser(String googleId) async {
    if (!await checkConnection()) {
      throw NoNetworkException();
    } else {
      try {
        final result = await _serviceClient.deleteUser(
          DeleteRequest()..googleId = googleId,
          options: CallOptions(
            timeout: Duration(seconds: 3),
          ),
        );

        if (result.isDeleted) {
          // clearing shared prefs
          return await clearingPrefs();
        } else {
          return false;
        }
      } catch (e) {
        throw ServerErrorException(e.message);
      }
    }
  }

  @override
  Future<User> getUser(String googleId) async {
    if (!await checkConnection()) {
      throw NoNetworkException();
    } else {
      try {
        final user = await _serviceClient.getUser(
          GetRequest()..googleId = googleId,
          options: CallOptions(
            timeout: Duration(seconds: 3),
          ),
        );

        await storeUserToPrefs(user);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setBool("isCwSignIn", true);
        return user;
      } catch (e) {
        throw ServerErrorException(e.message);
      }
    }
  }

  @override
  Future<User> updateUser(User user) async {
    if (!await checkConnection()) {
      throw NoNetworkException();
    } else {
      try {
        await _serviceClient.updateUser(
          user,
          options: CallOptions(
            timeout: Duration(seconds: 3),
          ),
        );
        await storeUserToPrefs(user);
        return user;
      } catch (e) {
        throw ServerErrorException(e.message);
      }
    }
  }

  @override
  Future<LinkedDevice> addLinkedDevice(LinkedDevice linkedDevice) async {
    if (!await checkConnection()) {
      throw NoNetworkException();
    } else {
      try {
        // check if the device is already linked or not
        bool isLinked = await isDeviceLinked(linkedDevice);
        if (isLinked) {
          return linkedDevice;
        } else {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          // add to server
          final result = await _serviceClient.addTvDevice(
            TvDevice()
              ..googleId = prefs.getString("googleId")
              ..linkedDevice = linkedDevice,
            options: CallOptions(
              timeout: Duration(seconds: 3),
            ),
          );
          result.linkedDevices.forEach((element) {
            //add locally
            addDeviceToPrefs(element);
          });
          return linkedDevice;
        }
      } catch (e) {
        throw ServerErrorException(e.message);
      }
    }
  }

  @override
  Future<bool> removeLinkedDevice(String tvEmac) async {
    if (!await checkConnection()) {
      throw NoNetworkException();
    } else {
      try {
        // remove from server
        SharedPreferences prefs = await SharedPreferences.getInstance();
        final result = await _serviceClient.removeTvDevice(
          RemoveTvDeviceRequest()
            ..googleId = prefs.getString("googleId")
            ..tvEmac = tvEmac,
          options: CallOptions(
            timeout: Duration(seconds: 3),
          ),
        );
        if (result.isTvDeviceRemoved) {
          //removed from local prefs
          return await removeDeviceFromPrefs(tvEmac);
        }
        return false;
      } catch (e) {
        throw ServerErrorException(e.message);
      }
    }
  }

  @override
  Future<List<LinkedDevice>> getLinkedDevices(String googleId) async {
    if (!await checkConnection()) {
      throw NoNetworkException();
    } else {
      try {
        // check locally first
        List<LinkedDevice> result = await getLinkedDevicesFromPrefs();
        if (result.isEmpty) {
          final serverResult = await _serviceClient.getLinkedDevices(
            GetRequest()..googleId = googleId,
            options: CallOptions(
              timeout: Duration(seconds: 3),
            ),
          );
          if (serverResult.isLinkedDeviceFetched &&
              serverResult.linkedDevices.isNotEmpty) {
            // store the devices locally.
            serverResult.linkedDevices.forEach(
              (element) {
                addDeviceToPrefs(element);
              },
            );
            //returning result
            return serverResult.linkedDevices;
          }
        }
        return result;
      } catch (e) {
        throw ServerErrorException(e.message);
      }
    }
  }
}
