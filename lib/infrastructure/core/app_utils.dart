import 'dart:io';
import 'package:cloudwalker_companion/domain/core/exceptions/no_user_locally_exeception.dart';
import 'package:cloudwalker_companion/domain/cw_registration/models/protos/User.pbgrpc.dart';
import 'package:cloudwalker_companion/domain/cw_registration/models/protos/google/type/date.pb.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<bool> checkConnection() async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  } on SocketException catch (_) {
    return false;
  }
}

Future<bool> clearingPrefs() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.clear();
}

Future<User> getUserFromPrefs() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();

  final isUserLocallyPresent = prefs.getBool("isUserLocallyPresent");

  if (isUserLocallyPresent != null && isUserLocallyPresent) {
    // making user from pref
    User user = User();

    user.name =
        prefs.getString("firstName") + " " + prefs.getString("lastName");
    user.email = prefs.getString("email");
    user.googleId = prefs.getString("googleId");

    user.phoneNumber = prefs.getString("phoneNumber");
    user.imageUrl = prefs.getString("imageUrl");
    user.genre.clear();
    user.genre.addAll(prefs.getStringList("genre"));
    user.language.clear();
    user.language.addAll(prefs.getStringList("languages"));
    user.contentType.clear();
    user.contentType.addAll(prefs.getStringList("contentType"));

    user.gender =
        prefs.getString("gender") == 'male' ? Gender.MALE : Gender.FEMALE;

    List<String> tmp = prefs.getString("dateOfBirth").split('-');
    Date pbDate = Date();
    pbDate.day = int.parse(tmp[0]);
    pbDate.month = int.parse(tmp[1]);
    pbDate.year = int.parse(tmp[2]);
    user.dateOfBirth = pbDate;

    final savedLinkedDevices = prefs.getStringList("linkedDevices");
    if (savedLinkedDevices != null && savedLinkedDevices.length > 0) {
      List<LinkedDevice> finalLd = List();
      savedLinkedDevices.forEach((element) {
        List<String> tmp = element.split('~');
        LinkedDevice ld = LinkedDevice();
        ld.tvEmac = tmp[0];
        ld.tvPanel = tmp[1];
        ld.tvBoard = tmp[2];
        ld.tvName = tmp[3];
        finalLd.add(ld);
      });
      user.linkedDevices.addAll(finalLd);
    }
    return user;
  } else {
    throw NoUserLocallyException();
  }
}

Future<bool> isDeviceLinked(LinkedDevice linkedDevice) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  List<String> ld = prefs.getStringList("linkedDevices");
  if (ld != null && ld.length > 0) {
    String tmp =
        "${linkedDevice.tvEmac}~${linkedDevice.tvPanel}~${linkedDevice.tvBoard}~${linkedDevice.tvName}";
    bool found = false;
    ld.forEach(
      (element) {
        if (element == tmp) {
          found = true;
        }
      },
    );
    return found;
  } else {
    return false;
  }
}

Future<bool> addDeviceToPrefs(LinkedDevice linkedDevice) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final isUserLocallyPresent = prefs.getBool("isUserLocallyPresent");
  if (isUserLocallyPresent != null && isUserLocallyPresent) {
    final found = await isDeviceLinked(linkedDevice);
    if (found) {
      return false;
    } else {
      List<String> ld = prefs.getStringList("linkedDevices");
      if (ld == null) {
        ld = List();
      }
      String tmp =
          "${linkedDevice.tvEmac}~${linkedDevice.tvPanel}~${linkedDevice.tvBoard}~${linkedDevice.tvName}";
      ld.add(tmp);
      prefs.setStringList("linkedDevices", ld);
      return true;
    }
  } else {
    throw NoUserLocallyException();
  }
}

Future<bool> removeDeviceFromPrefs(String tvEmac) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final isUserLocallyPresent = prefs.getBool("isUserLocallyPresent");
  if (isUserLocallyPresent != null && isUserLocallyPresent) {
    List<String> ld = prefs.getStringList("linkedDevices");
    if (ld != null && ld.length > 0) {
      int targetIndex = -1;
      ld.forEach(
        (element) {
          if (element.contains(tvEmac)) {
            targetIndex = ld.indexOf(element);
          }
        },
      );
      if (targetIndex >= 0) {
        ld.removeAt(targetIndex);
      }
      return prefs.setStringList("linkedDevices", ld);
    } else {
      return true;
    }
  } else {
    throw NoUserLocallyException();
  }
}

Future<List<LinkedDevice>> getLinkedDevicesFromPrefs() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final isUserLocallyPresent = prefs.getBool("isUserLocallyPresent");
  if (isUserLocallyPresent != null && isUserLocallyPresent) {
    List<String> tmp = prefs.getStringList("linkedDevices");
    if (tmp != null && tmp.length > 0) {
      List<LinkedDevice> result = List();
      tmp.forEach(
        (element) {
          LinkedDevice linkedDevice = LinkedDevice();
          List<String> s = element.split('~');
          linkedDevice.tvEmac = s[0];
          linkedDevice.tvPanel = s[1];
          linkedDevice.tvBoard = s[2];
          linkedDevice.tvName = s[3];
          result.add(linkedDevice);
        },
      );
      return result;
    } else {
      return List<LinkedDevice>();
    }
  } else {
    throw NoUserLocallyException();
  }
}

Future<bool> storeUserToPrefs(User user) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final isGoogleSignedIn = prefs.getBool("isGoogleSignIn");
  if (isGoogleSignedIn != null && isGoogleSignedIn) {
    prefs.setString("firstName", user.name.split(" ")[0]);

    prefs.setString("lastName", user.name.split(" ")[1]);

    prefs.setString("dateOfBirth",
        "${user.dateOfBirth.day.toString()}-${user.dateOfBirth.month.toString()}-${user.dateOfBirth.year.toString()}");

    prefs.setString("gender", user.gender.name.toLowerCase());

    prefs.setString("phoneNumber", user.phoneNumber);

    prefs.setString("email", user.email);

    prefs.setString("imageUrl", user.imageUrl);

    prefs.setStringList("genre", user.genre.toList());

    prefs.setStringList("languages", user.language.toList());

    prefs.setStringList("contentType", user.contentType.toList());

    if (user.linkedDevices != null && user.linkedDevices.length > 0) {
      List<String> tmp = List();
      user.linkedDevices.forEach(
        (element) {
          tmp.add(
              "${element.tvEmac}~${element.tvPanel}~${element.tvBoard}~${element.tvName}");
        },
      );

      prefs.setStringList("linkedDevices", tmp);
    }
    //important
    prefs.setBool("isUserLocallyPresent", true);
    return true;
  } else {
    prefs.setBool("isUserLocallyPresent", false);
    return false;
  }
}
