import 'package:cloudwalker_companion/domain/cw_registration/models/protos/User.pbgrpc.dart';
import 'package:cloudwalker_companion/domain/search/models/protos/CDEService.pbgrpc.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:grpc/grpc.dart';
import 'package:injectable/injectable.dart';
import 'package:multicast_dns/multicast_dns.dart';
import "package:grpc/src/client/http2_channel.dart" as chan;

@module
abstract class AppInjectableModule {
  @lazySingleton
  UserServiceClient get userServiceClient {
    return UserServiceClient(
      chan.ClientChannel(
        "node43329-cw-profile.cloudjiffy.net",
        port: 11095,
        options:
            const ChannelOptions(credentials: ChannelCredentials.insecure()),
      ),
    );
  }

  // @lazySingleton
  // CDEServiceClient get cdeServiceClient {
  //   return CDEServiceClient(
  //     chan.ClientChannel(
  //       "node43754-search-service.cloudjiffy.net",
  //       port: 11056,
  //       options:
  //           const ChannelOptions(credentials: ChannelCredentials.insecure()),
  //     ),
  //   );
  // }

  @lazySingleton
  CDEServiceClient get cdeServiceClient {
    return CDEServiceClient(
      chan.ClientChannel(
        "192.168.0.107",
        port: 7771,
        options:
            const ChannelOptions(credentials: ChannelCredentials.insecure()),
      ),
    );
  }

  @lazySingleton
  GoogleSignIn get googleSignIn => GoogleSignIn();

  @lazySingleton
  MDnsClient get mdnsClient => MDnsClient();
}
