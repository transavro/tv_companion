import "package:grpc/src/client/http2_channel.dart" as chan;
import 'package:connectivity/connectivity.dart';
import 'package:grpc/grpc.dart';
import 'package:grpc/service_api.dart';
import 'package:injectable/injectable.dart';

import 'package:cloudwalker_companion/domain/core/exceptions/server_error_exception.dart';
import 'package:cloudwalker_companion/domain/tv_interaction/exceptions/no_wifi_exception.dart';
import 'package:cloudwalker_companion/domain/tv_interaction/i_tv_interaction_facade.dart';
import 'package:cloudwalker_companion/domain/tv_interaction/models/protos/TvInteraction.pb.dart';
import 'package:cloudwalker_companion/domain/tv_interaction/models/protos/TvInteraction.pbgrpc.dart';

@LazySingleton(as: ITvInteractionFacade)
class TvInteractionFacadeImpl implements ITvInteractionFacade {
  TvInteractionServiceClient _tvServiceClient;
  final Connectivity _connectivity = Connectivity();

  TvInteractionFacadeImpl();

  @override
  Future<TvInteractionServiceClient> connectToTv(String ip, int port) async {
    _tvServiceClient = TvInteractionServiceClient(chan.ClientChannel(
      ip,
      port: port,
      options: const ChannelOptions(credentials: ChannelCredentials.insecure()),
    ));
    return _tvServiceClient;
  }

  @override
  Future<bool> appActionTrigger(AppReq appReq) async {
    if (!await isWifiConnected()) {
      throw NoWifiException();
    } else {
      try {
        await _tvServiceClient.appAction(
          appReq,
          options: CallOptions(
            timeout: Duration(seconds: 3),
          ),
        );
        return true;
      } catch (e) {
        throw ServerErrorException(e);
      }
    }
  }

  @override
  Future<GetAllAppsResp> getAllTvApps(GetAllAppsReq getAllAppsReq) async {
    if (!await isWifiConnected()) {
      throw NoWifiException();
    } else {
      try {
        return await _tvServiceClient.getAllApps(
          getAllAppsReq,
          options: CallOptions(
              // timeout: Duration(seconds: 4),
              ),
        );
      } catch (e) {
        throw ServerErrorException(e);
      }
    }
  }

  @override
  Future<TvDataResp> getTvData(String deviceName) async {
    if (!await isWifiConnected()) {
      throw NoWifiException();
    } else {
      try {
        return await _tvServiceClient.getTvData(
          TvDataReq()..targetModel = deviceName,
          options: CallOptions(
            timeout: Duration(seconds: 3),
          ),
        );
      } catch (e) {
        throw ServerErrorException(e);
      }
    }
  }

  @override
  Future<List<String>> getTvSources() async {
    if (!await isWifiConnected()) {
      throw NoWifiException();
    } else {
      try {
        final result = await _tvServiceClient.getListOfTvSources(
          Req(),
          options: CallOptions(
            timeout: Duration(seconds: 3),
          ),
        );
        return result.tvsources;
      } catch (e) {
        throw ServerErrorException(e);
      }
    }
  }

  @override
  Future<bool> keycodeTrigger(KeycodeReq keycodeReq) async {
    if (!await isWifiConnected()) {
      throw NoWifiException();
    } else {
      try {
        await _tvServiceClient.keycodeAction(
          keycodeReq,
          options: CallOptions(
            timeout: Duration(seconds: 3),
          ),
        );
        return true;
      } catch (e) {
        throw ServerErrorException(e);
      }
    }
  }

  @override
  Future<bool> playContentTrigger(PlayReq playReq) async {
    if (!await isWifiConnected()) {
      throw NoWifiException();
    } else {
      try {
        await _tvServiceClient.playContent(
          playReq,
          options: CallOptions(
            timeout: Duration(seconds: 3),
          ),
        );
        return true;
      } catch (e) {
        throw ServerErrorException(e);
      }
    }
  }

  @override
  Future<bool> switchToTvSource(String sourceName) async {
    if (!await isWifiConnected()) {
      throw NoWifiException();
    } else {
      try {
        await _tvServiceClient.tvAction(
          TvActionReq()..source = sourceName,
          options: CallOptions(
            timeout: Duration(seconds: 3),
          ),
        );
        return true;
      } catch (e) {
        throw ServerErrorException(e);
      }
    }
  }

  Future<bool> isWifiConnected() async {
    final connectivityResult = await _connectivity.checkConnectivity();
    if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }
}
