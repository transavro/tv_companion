import 'package:cloudwalker_companion/presentation/tv_remote/cwKeycode.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

import 'injection.dart';
import 'presentation/core/app_widget.dart';

void main() async {
  loadKeyCodeMap();
  configureInjection(Environment.prod);
  runApp(AppWidget());
}
